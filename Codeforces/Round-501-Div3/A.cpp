#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef long long ll;

typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

int main(){
	ios_base::sync_with_stdio(false);

	int n, m;
	cin >> n >> m;
	int vis[m + 1];
	memset(vis, 0, sizeof vis);

	for(int i = 0; i < n; i++){
		int x, y;
		cin >> x >> y;
		for(int j = x; j <= y; j++){
			vis[j] = 1;
		}
	}
	int r = 0;
	vi v;
	for(int i = 1; i <= m; i++){
		if(!vis[i]) r++, v.push_back(i);
	}
	cout << r << endl;
	for(int u : v){
		cout << u << " ";
	}

	return 0;
}
