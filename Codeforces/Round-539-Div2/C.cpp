#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

ll vet[300005], pref[300005];
map<ll,vector<int>> par, impar;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n; cin >> n;
    for(int i = 1; i <= n; i++) cin >> vet[i];
    ll tot = 0;
    for(int i = 1; i <= n; i++){
        pref[i] = pref[i - 1] ^ vet[i];
        if(i % 2 == 0) par[pref[i]].push_back(i);
        else impar[pref[i]].push_back(i);
    }
    ll r = 0;
    for(int i = 1; i <= n; i++){
        ll ant = pref[i - 1];
        if(i % 2 == 0){
            int p = upper_bound(impar[ant].begin(), impar[ant].end(), i) - impar[ant].begin();
            r += (impar[ant].size() - p);
        } else {
            int p = upper_bound(par[ant].begin(), par[ant].end(), i) - par[ant].begin();
            r += (par[ant].size() - p);
        }
    }
    cout << r << endl;

    return 0;
}