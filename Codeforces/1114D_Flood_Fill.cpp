#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int vet[5005];
int dp[5005][5005];
int k = 0;

int solve(int i, int j){
    if(i == 0 and j == k - 1) return 0;
    if(dp[i][j] != -1) return dp[i][j];
    int res = 1e9;
    if(i - 1 >= 0 and j + 1 < k and vet[i - 1] == vet[j + 1]) res = min(res, solve(i - 1, j + 1) + 1);
    if(i - 1 >= 0) res = min(res, solve(i - 1, j) + 1);
    if(j + 1 < k) res = min(res, solve(i, j + 1) + 1);
    return dp[i][j] = res;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int n; cin >> n;
    int aux[5005];
    for(int i = 0; i < n; i++){
        cin >> aux[i];
    }
    int last_c = aux[0];
    for(int i = 0; i < n; i++){
        if(aux[i] != last_c){
            vet[k++] = last_c;
        }
        last_c = aux[i];
    }
    vet[k++] = last_c;
    int res = 1e9;
    memset(dp, -1, sizeof dp);
    for(int i = 0; i < k; i++){
        res = min(res, solve(i,i));
    }
    cout << res << endl;
    
    return 0;
}