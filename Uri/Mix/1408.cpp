#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef tuple<int,int,int> iii;
typedef tuple<int,int,int,int> i4;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    int n, m;
    while(cin >> n >> m and (n + m != 0)){
        vector<int> vet;
        vector<ii> starts;
        for(int i = 0; i < m; i++){
            int x; cin >> x;
            if(!i) starts.push_back({x, i+1});
            else {
                if(vet.back() + 1 != x){
                    starts.push_back({x, i+1});
                }
            }
            vet.push_back(x);

        }
        int res = 1e9;
        for(ii p : starts){
            int u = p.first;
            int idx = p.second;
            int pos;
            if(binary_search(vet.begin(), vet.end(), (u + n - 1))){
                pos = lower_bound(vet.begin(), vet.end(), (u + n - 1)) - vet.begin() + 1;
            } else {
                pos = lower_bound(vet.begin(), vet.end(), (u + n - 1)) - vet.begin();
            }
            res = min(res, n - (pos - idx + 1));
        }
        cout << res << endl;
    }

    return 0;
}