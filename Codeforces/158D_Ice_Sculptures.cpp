#include <bits/stdc++.h>
using namespace std;

int v[20005];
int n;

int simulacao(int k){
    int best = -1e9;
    for(int i = 0; i < k; i++){
        int aux = 0;
        int cnt = 0;
        for(int j = i; j < n; j += k){
            aux += v[j];
            cnt++;
        }
        if(cnt >= 3) best = max(aux, best);
    }
    return best;
}

int main(){

    cout.sync_with_stdio(0);
    cin.tie(0);

	cin >> n;
    int res = 0;
    for(int i = 0; i < n; i++)
        cin >> v[i], res += v[i];

    int root = sqrt(n);
    for(int k = 2; k <= root; k++)
        if(n%k == 0){
            int r1 = k, r2 = n/k;
            res = max(res, simulacao(r1));
            res = max(res, simulacao(r2));
        }
    cout << res << endl;
	return 0;
}