#include <bits/stdc++.h>
using namespace std;

struct quadrado{
    int x, y, xx, yy;
    
    quadrado(int _x, int _y, int _xx, int _yy){
        x = _x; y = _y; xx = _xx; yy = _yy;
    }
    bool operator <(const quadrado& other) const{
        long long a1 = 1LL*abs(y-yy)*abs(x-xx);
        long long a2 = 1LL*abs(other.y-other.yy)*abs(other.x-other.xx);
        return a1 < a2;
    }
};

map<int, int> BIT[400005];

int query(int x, int y){
    int val = 0;
    for(int i = x + 1; i > 0; i -= (i & -i)){
        for(int j = y + 1; j > 0; j -= (i & -i)){
            val += BIT[i][j];
        }
    }
    return val;
}

void update(int x, int y, int val){
    for(int i = x + 1; i <= 400005; i += (i & -i)){
        for(int j = y + 1; j <= 400005; j += (j & -j)){
            BIT[i][j] += val;
        }
    }
}

// .......
// ..*....
// .......
// ......*
// .......


void update_aux(int x, int y, int xx, int yy){
    swap(x, xx);
    
    update(x, y, 1);
    update(xx - 1, y, -1);
    update(x, yy + 1, -1);
    update(xx - 1, yy + 1, 1);
}

int query_aux(int x, int y, int xx, int yy){
    swap(x, xx);

    int first = query(x, y);
    int second = query(xx - 1, y);
    int third = query(x, yy + 1);
    int fourth = query(xx - 1, yy + 1);
    return (first - second - third + fourth > 0);
}

int main(){
    map<int,int> mapa;

    map<int,int> arvore[100005];

    vector<quadrado> vec;
    int n; scanf("%d", &n);
    for(int i = 0; i < n; i++){
        int x, y, xx, yy;
        mapa[x] = 1; mapa[y] = 1; mapa[xx] = 1; mapa[yy] = 1;
        scanf("%d %d %d %d", &x, &y, &xx, &yy);
        vec.push_back(quadrado(x,y,xx,yy));
    }
    sort(vec.begin(), vec.end());

    int pos = 1;
    for(auto& par : mapa)
        par.second = pos++;
    for(auto par : mapa)
        printf("%d %d\n", par.first, par.second);
    
    int res = 0;
    for(int i = 0; i < n; i++){
        quadrado q = vec[i];
        int x = q.x, y = q.y, xx = q.xx, yy = q.yy;
        x = mapa[x]; y = mapa[y]; xx = mapa[xx]; yy = mapa[yy];
        if(query_aux(x,y,xx,yy) == 0) res++;

        update_aux(x,y,xx,yy);
        
    }
    printf("%d\n", res);
    return 0;
}