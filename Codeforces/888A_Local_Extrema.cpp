#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min (a,b) ((a) < (b) ? (a) : (b))
#define max (a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

int main(){
    int n; cin >> n;
    int vet[n + 5];
    for(int i = 0; i < n; i++) cin >> vet[i];
    int res = 0;
    for(int i = 1; i < n - 1; i++){
        if(vet[i] > vet[i - 1] and vet[i] > vet[i + 1]) res++;
        else if(vet[i] < vet[i - 1] and vet[i] < vet[i + 1]) res++;
    }
    cout << res << endl;
    
    return 0;
}