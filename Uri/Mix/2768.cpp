#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3f
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int dist[105][105][105];

int main(){
    // ios_base::sync_with_stdio(false);
    // cin.tie(0);
    
    int n, m;
    while(scanf("%d %d", &n, &m) != EOF){
        memset(dist, INF, sizeof dist);
        for(int i = 1; i <= n; i++){
            for(int k = 1; k <= n; k++) dist[i][i][k] = 0;
        }
        for(int i = 0; i < m; i++){
            int x, y, w; scanf("%d %d %d", &x, &y, &w);
            for(int k = 1; k <= n; k++){
                dist[x][y][k] = dist[y][x][k] = w;
            }
        }

        for(int k = 1; k <= n; k++){
            for(int i = 1; i <= n; i++){
                for(int j = 1; j <= n; j++){
                    dist[i][j][k] = min(dist[i][j][k], dist[i][j][k-1]);
                    dist[i][k][k] = min(dist[i][k][k], dist[i][k][k-1]);
                    dist[k][j][k] = min(dist[k][j][k], dist[k][j][k-1]);
                    dist[i][j][k] = min(dist[i][j][k], dist[i][k][k] + dist[k][j][k]);
                    // }
                }
            }
        }

        int q; cin >> q;
        while(q--){
            int a, b, c; cin >> a >> b >> c;
            if(dist[a][b][c] == INF) printf("-1\n");
            else printf("%d\n", dist[a][b][c]);
        }
        
    }

    
    return 0;
}