#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef long long ll;

typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
int n;

int main(){
	ios_base::sync_with_stdio(false);

	cin >> n;
	vector<pair<int, string> > vs;
	for(int i = 0; i < n; i++){
		string s;
		cin >> s;
		vs.push_back({s.size(), s});
	}
	sort(vs.begin(), vs.end());
	int f = 0;
	for(int i = 1; i < n; i++){
		if(vs[i].second.find(vs[i - 1].second) == -1) f = 1;
	}
	if(!f){
		cout << "YES" << endl;
		for(pair<int, string> &p : vs){
			cout << p.second << endl;
		}
	} else {
		cout << "NO" << endl;
	}

	return 0;
}
