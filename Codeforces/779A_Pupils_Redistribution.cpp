#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int vetA[6], vetB[6];

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);

    int n;
    cin >> n;
    for(int i = 0; i < n; i++){
        int x;
        cin >> x;
        vetA[x]++;
    }
    for(int i = 0; i < n; i++){
        int x;
        cin >> x;
        vetB[x]++;
    }
    int resp = 0;
    int f = 1;
    for(int i = 1; i <= 5 and f; i++){
        if((vetA[i] + vetB[i]) & 1) f = 0;
        else {
            if(vetA[i] != vetB[i]){
                int dif = abs(vetA[i] - vetB[i]) / 2;
                resp += dif;
                if(vetA[i] > vetB[i]){
                    vetA[i] -= dif;
                    vetB[i] += dif;
                    for(int j = i + 1; j <= 5 and dif > 0; j++){
                        while(vetA[j] < vetB[j] and dif > 0){
                            vetA[j]++;
                            vetB[j]--;
                            dif--;
                        }
                    }
                    if(dif > 0) f = 0;
                } else {
                    vetA[i] += dif;
                    vetB[i] -= dif;
                    for(int j = i + 1; j <= 5 and dif > 0; j++){
                        while(vetA[j] > vetB[j] and dif > 0){
                            vetA[j]--;
                            vetB[j]++;
                            dif--;
                        }
                    }
                    if(dif > 0) f = 0;
                }
            }
        }
    }
    if(!f) cout << "-1" << endl;
    else cout << resp << endl;
    return 0;
}
