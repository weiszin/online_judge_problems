#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef long long ll;

typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

int n;
string s;

int a[123456], dp[123456][5];

int solve(int pos, int flag){
    if(pos == n) return 0;
    if(dp[pos][flag] != -1) return dp[pos][flag];
    int res = 0;
    if(s[pos] == '1'){
        int esq = solve(pos+1, flag) + a[pos];
        int dir = solve(pos+1, 1);
        res = max(esq, dir);
    } else {
        int esq = (flag ? solve(pos + 1, 1) + a[pos] : -1e9);
        int dir = solve(pos + 1, flag);
        res = max(esq, dir);
    }
    return dp[pos][flag] = res;
}

int main(){
	cout.sync_with_stdio(false); cin.tie(nullptr);
    memset(dp, -1, sizeof dp);
    cin >> n;
    for(int i = 0; i < n; i++)
        cin >> a[i];
    reverse(a, a + n);
    cin >> s;
    reverse(s.begin(), s.end());
    cout << solve(0,0) << endl;

	return 0;
}
