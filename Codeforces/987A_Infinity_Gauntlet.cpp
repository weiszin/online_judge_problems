#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

map<string, int> mapa;

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);

    int n;
    cin >> n;

    string res[6] = {"Power", "Time", "Space", "Soul", "Reality", "Mind"};
    int vis[6];
    memset(vis, 0, sizeof vis);
    mapa["purple"] = 0;
    mapa["green"] = 1;
    mapa["blue"] = 2;
    mapa["orange"] = 3;
    mapa["red"] = 4;
    mapa["yellow"] = 5;

    for(int i = 0; i < n; i++){
        string s;
        cin >> s;
        vis[mapa[s]] = 1;
    }
    cout << 6 - n << endl;
    for(int i = 0; i < 6; i++){
        if(!vis[i]){
            cout << res[i] << endl;
        }
    }

    return 0;
}
