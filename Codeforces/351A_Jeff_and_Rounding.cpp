#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;
typedef pair<double,double> pdd;

int n;
double v[4005];
pdd dp[4005][4005];

pdd solve(int pos, int k){
    if(pos == n) return (k == 0 ? pdd(0,0) : pdd(1e9,0));
    if(k+n/2 < 0 || k+n/2 > n) return pdd(1e9, 0);
    if(dp[pos][k+n/2] != pdd(-1,-1)) return dp[pos][k+n/2];
    pdd r1 = solve(pos+1, k+1);
    r1.first += (ceil(v[pos])-v[pos]);
    pdd r2 = solve(pos+1, k-1);
    r2.second += (v[pos] - floor(v[pos]));
    if(fabs(r1.first-r1.second) < fabs(r2.first-r2.second))
        return dp[pos][k+n/2] = r1;
    else
        return dp[pos][k+n/2] = r2;
}

int main(){
    // ios_base::sync_with_stdio(false);
    // cin.tie(0);
    
    cin >> n;
    n *= 2;
    for(int i = 0; i <= n; i++)
        for(int j = 0; j <= 2*n; j++)
            dp[i][j] = pdd(-1,-1);

    for(int i = 0; i < n; i++)
        cin >> v[i];
    pdd res = solve(0,0);
    printf("%.3f\n", fabs(res.first-res.second));
    return 0;
}