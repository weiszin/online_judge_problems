#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

// vector<int> mx[25][25];
int maxi_tempo = 0;
int dx[5] = {0, 0, -1, 1, 0};
int dy[5] = {-1, 1, 0, 0, 0};
int dp[25][25][1005];
int mx[25][25][1005];

int valid(int x, int y){
    return (x >= 0 and x <= 20 and y >= 0 and y <= 20);
}

int solve(int x, int y, int t){
    if(t > maxi_tempo) return 0;
    if(dp[x][y][t] != -1) return dp[x][y][t];
    int res = mx[x][y][t];
    int aux = 0;
    for(int i = 0; i < 5; i++){
        int xx = x + dx[i];
        int yy = y + dy[i];
        if(valid(xx, yy)){
            aux = max(aux, solve(xx, yy, t + 1));
        }
    }
    return dp[x][y][t] = res + aux;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n; cin >> n;
    
    for(int i = 0; i < n; i++){
        int x, y, t;
        cin >> x >> y >> t;
        maxi_tempo = max(maxi_tempo, t);
        mx[x][y][t]++;
    }
    memset(dp, -1, sizeof dp);
    cout << solve(6, 6, 0) << endl;

    return 0;
}