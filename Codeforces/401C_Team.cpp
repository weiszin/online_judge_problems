#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);
    int n, m;
    cin >> n >> m;
    int f = 0;
    string res = "";

    if(2 * n + 2 < m) cout << -1 << endl;
    else if(n > m and n - m >= 2) cout << -1 << endl;
    else {
        if(m > n){
            int tmp = ceil(m / (double)n);
            if(tmp >= 3){
                cout << "11";
                m -= 2;
                while(n + m != 0){
                    if(n > 0){
                        cout << "0";
                        n--;
                    }
                    if(m > 0){
                        if(m - 2 >= n and m - 2 >= 0){
                            cout << "11";
                            m -= 2;
                        } else {
                            cout << "1";
                            m--;
                        }
                    }
                }
            } else {
                cout << "0";
                n--;
                while(n + m != 0){
                    if(m > 0){
                        if(m - 2 >= n and m - 2 >= 0){
                            cout << "11";
                            m -= 2;
                        } else {
                            cout << "1";
                            m--;
                        }
                    }
                    if(n > 0){
                        cout << "0";
                        n--;
                    }
                }
            }
            cout << endl;
        } else {
            for(int i = 0; i < n + m; i++){
                if(i & 1) cout << "1";
                else cout << "0";
            } cout << endl;
        }
    }

    return 0;
}
