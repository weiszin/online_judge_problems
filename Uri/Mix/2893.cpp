#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int mod = 1000007;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int t; cin >> t;
    while(t--){
        int k, n; cin >> k >> n;
        if(n <= k){
            cout << n - 1 << endl;
        } else {
            ll pref[n + 5]; memset(pref, 0LL, sizeof pref);
            ll fibb[n + 5]; memset(fibb, 0LL, sizeof fibb);
            for(ll i = 1; i <= k - 1; i++){
                pref[i] = (pref[i - 1] + i) % mod;
                fibb[i] = i;
            }
            fibb[k] = pref[k - 1] % mod;
            pref[k] = (2 * pref[k - 1])%mod;
            // cout << "fibb[" << k << "] = " << fibb[k] << endl;
            // cout << "pref[" << k << "] = " << pref[k] << endl;
            for(int i = k + 1; i <= n; i++){
                fibb[i] = (pref[i - 1] - pref[i - k - 1] + mod) % mod;
                pref[i] = (pref[i - 1] + fibb[i]) % mod;
                // cout << "fibb[" << i << "] = " << fibb[i] << endl;
                // cout << "pref[" << i << "] = " << pref[i] << endl;
            }
            cout << fibb[n-1] << endl;
        }
    }
    
    return 0;
}