#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

int vet[100005];
int suff[100005];
int ST[100005][20];
int n, l, r;

int query(int l, int r){
    int diff = (r - l + 1);
    int logg = (int)log2(diff);
    return max(ST[l][logg], ST[r - (1 << logg) + 1][logg]);
}

int dp[100005];

int solve(int pos){
    if(pos >= n) return 1e9;
    if(pos + r >= n) return query(pos, min(n - 1, pos + l));
    if(dp[pos] != -1) return dp[pos];
    int res = 0;
    for(int i = l; i <= r; i++){
        res = max(res, min(query(pos, pos + i - 1), solve(pos + i)));
    }
    return dp[pos] = res;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cin >> n >> l >> r;
    for(int i = 0; i < n; i++) cin >> vet[i];
    // suff[n - 1] = vet[n - 1];
    for(int i = n - 1; i >= 0; i--){
        suff[i] = max(suff[i + 1], vet[i]);
    }
    for(int i = 0; i < n; i++){
        ST[i][0] = vet[i];
    }
    for(int j = 1; j < 20; j++){
        for(int i = 0; i + (1 << j) <= n; i++){
            ST[i][j] = max(ST[i][j - 1], ST[i + (1 << (j - 1))][j - 1]);
        }
    }
    int res = 1e9;
    int cnt = 0;
    int i = 0;
    while(i < n){
        if(i + r >= n){
            res = min(res, suff[i]);
            break;
        }
        int flag = 0;
        for(int j = i; j < i + l; j++){
            if(suff[j] != suff[j + 1]) flag = 1;
        }
        if(flag){
            res = min(res, query(i, i + l - 1));
            i = i + l;
        } else {
            int k;
            for(k = l; k < r; k++){
                if(suff[i + k] != suff[i + k + 1]) break;
            }
            res = min(res, query(i, i + k));
            i = i + k + 1;
        }
    }
    cout << res << endl;
    // memset(dp, -1, sizeof dp);
    // cout << solve(0) << endl;

    return 0;
}