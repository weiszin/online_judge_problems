#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int a, b, c; cin >> a >> b >> c;
    cout << "A = " << a << ", B = " << b << ", C = " << c << endl;
    cout << "A = " << right << setw(10) << a << ", B = " << right << setw(10) << b << ", C = " << right << setw(10) << c << endl;
    cout << "A = " << setfill('0') << internal << setw(10) << a << ", B = " << setfill('0') << setw(10) << b << ", C = " << setfill('0') << setw(10) << c << endl;
    cout << "A = " << setfill(' ') << left << setw(10) << a << ", B = " << setfill(' ') << left << setw(10) << b << ", C = " << setfill(' ') << left << setw(10) << c << endl;
    
    return 0;
}