#include <bits/stdc++.h>

using namespace std;

#define max(a,b) ((a) > (b) ? (a) : (b))

int main(){
    int m;
    while(cin >> m and m){
        cin.ignore();
        string s; getline(cin, s);
        int res = 0;
        map<char, int> mapa;
        deque<char> dq;
        for(char c : s){
            dq.push_back(c);
            mapa[c]++;
            while(mapa.size() > m){
                char c = dq.front(); dq.pop_front();
                mapa[c]--;
                if(mapa[c] == 0){
                    mapa.erase(c);
                }
            }
            res = max(res, dq.size());
        }
        cout << res << endl;
    }
}
