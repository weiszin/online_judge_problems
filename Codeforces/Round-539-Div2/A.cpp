#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n, v; cin >> n >> v;

    int res = 0;
    int pos = 1;
    int t = 0;
    while(pos < n){
        if(pos + t == n){
            cout << res << endl;
            return 0;
        }
        int dist = n - pos;
        res += min(v - t, dist) * pos;
        t += min(v - t, dist);
        pos++;
        t--;
    }
    cout << res << endl;
    
    return 0;
}