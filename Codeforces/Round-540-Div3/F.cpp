#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int c[300005], cnt[300005][5];
vector<vi> adj;

void dfs(int u, int p){
    cnt[u][c[u]]++;
    for(int v : adj[u])
        if(v != p){
            dfs(v, u);
            for(int j = 0; j < 3; j++)
                cnt[u][j] += cnt[v][j];
        }
}

int solve(int u, int p){
    int res = 0;
    for(int v : adj[u])
        if(v != p){
            res += solve(v, u);
            int aux[3];
            for(int j = 0; j < 3; j++)
                aux[j] = cnt[1][j] - cnt[v][j];
            if(!((aux[1] and aux[2]) || (cnt[v][1] and cnt[v][2])))
                res++;
        }
    return res;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    int n; cin >> n;
    adj.assign(n+5, vi());

    for(int i = 1; i <= n; i++)
        cin >> c[i];
    for(int i = 1; i <= n-1; i++){
        int u, v; cin >> u >> v;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }
    dfs(1, 0);
    cout << solve(1, 0) << endl;
    
    return 0;
}