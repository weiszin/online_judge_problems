#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))
#define MOD 100000000

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int ESQ = 0, DIR = 1;

int k1, k2;

int dp[105][105][15][2];

int solve(int n, int m, int seg, int flag){
    if(n + m == 0) return 1;
    int& ddp = dp[n][m][seg][flag];
    if(ddp != -1) return ddp;
    int res = 0;
    if(n > 0){
        if(flag == ESQ and seg + 1 <= k1){
            res = (res + solve(n - 1, m, seg + 1, ESQ) % MOD) % MOD;
        } else if(flag == DIR){
            res = (res + solve(n - 1, m, 1, ESQ) % MOD) % MOD;
        }
    }
    if(m > 0){
        if(flag == DIR and seg + 1 <= k2){
            res = (res + solve(n, m - 1, seg + 1, DIR) % MOD) % MOD;
        } else if(flag == ESQ){
            res = (res + solve(n, m - 1, 1, DIR) % MOD) % MOD;
        }
    }
    return ddp = res;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int n1, n2;
    cin >> n1 >> n2 >> k1 >> k2;
    memset(dp, -1, sizeof dp);
    cout << solve(n1, n2, 0, ESQ) << endl;
    
    return 0;
}