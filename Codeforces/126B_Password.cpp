#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int mod = 1000000007;
int p = 307;

int expo(int b, int e){
    int res = 1;
    while(e > 0){
        if(e & 1){
            res = ((ll)res * (ll)b) % mod;
            e--;
        }
        e /= 2;
        b = ((ll)b * (ll)b) % mod;
    }
    return res;
}

int preff[1000005];

int fft(int i, int j){
    int f = preff[i];
    int s = ((ll)expo(p, j - i + 1) * preff[j + 1]) % mod;
    return (f - s + mod) % mod;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    string s; cin >> s;
    s = "$" + s;
    
    for(int i = s.size(); i >= 1; i--){
        preff[i] = (s[i] + ((ll)p * preff[i + 1]) % mod) % mod;
    }

    for(int i = 1; i <= s.size() - 2; i++){
        int pref = fft(1, s.size() - i - 1);
        int suf = fft(i + 1, s.size() - 1);
        if(pref == suf){            
            // string a1 = s.substr(1, s.size() - i - 1);
            // string a2 = s.substr(i + 1, s.size() - i - 1);
            // if(a1 == a2){
                int tam = s.size() - i - 2;
                for(int j = 2; j <= s.size() - 1; j++){
                    if(j + tam >= s.size() - 1) break;
                    int sub = fft(j, j + tam);
                    if(sub == pref){
                        // string a3 = s.substr(j, tam + 1);
                        // if(a3 == a1){
                            cout << s.substr(j, tam + 1) << endl;
                            return 0;
                        // }
                    }
                }
            // }
        }
    }
    cout << "Just a legend" << endl;

    return 0;
}