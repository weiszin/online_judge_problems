#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

ll mod = 1e9+7;

ll expo(ll b, ll e){
    ll res = 1;
    while(e > 0){
        if(e & 1){
            res = (res * b) % mod;
            e--;
        }
        e /= 2;
        b = (b * b) % mod;
    }
    return res;
}

ll fat[100005];
ll invFat[100005];
int cnt[300];

void initFat(){
    fat[0] = 1;
    invFat[0] = 1;
    for(ll i = 1; i <= 100000; i++){
        fat[i] = (fat[i - 1] * i) % mod;
        invFat[i] = expo(fat[i], mod - 2);
        // cout << i << " " << invFat[i] << endl;
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    string s; cin >> s;
    initFat();
    ll cima = fat[s.size()];
    ll baixo = 1;
    for(char c : s) cnt[c]++;
    for(char c = 'a'; c <= 'z'; c++){
        baixo = (baixo * invFat[cnt[c]]) % mod;
    }
    ll res = (cima * baixo) % mod;
    cout << res << endl;
    
    return 0;
}