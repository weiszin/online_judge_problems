#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

vector<vector<int> > adj;
int respect[100005];
vector<int> fila;

int delete_v(int u){
    if(respect[u] == 0) return 0;
    for(int v : adj[u]){
        if(respect[v] == 0) return 0;
    }
    return 1;
}

void dfs(int u){
    if(delete_v(u)) fila.push_back(u);
    for(int v : adj[u]){
        dfs(v);
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int n; cin >> n;
    adj.assign(n + 5, vector<int>());
    int root;
    for(int i = 1; i <= n; i++){
        int x, y; cin >> x >> y;
        if(x != -1){
            adj[x].push_back(i);
        } else {
            root = i;
        }
        respect[i] = y;
    }

    dfs(root);
    if(fila.empty()){
        cout << -1 << endl;
        return 0;
    }
    sort(fila.begin(), fila.end());
    for(int u : fila) cout << u << " ";
    cout << endl;
    
    return 0;
}