#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef tuple<int,int,int> i3;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n;
    while(cin >> n and n){
        int res = 0;
        int cnt = 0;
        for(int i = 0; i < n; i++){
            int a, b; cin >> a >> b;
            b /= 2;
            res += (b+cnt) / 2;
            cnt = (cnt + b) % 2;            
        }
        cout << res << endl;
    }

    return 0;
}