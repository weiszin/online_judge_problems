#include <iostream>
#include <vector>

using namespace std;

vector<int> vet;

int isSorted(int l, int r){
    int last = -1;
    for(int i = l; i <= r; i++){
        if(vet[i] < last) return 0;
        last = vet[i];
    }
    return 1;
}

int solve(int l, int r){
    if(isSorted(l, r)) return (r - l + 1);
    int mid = (r + l + 1) / 2;
    return max(solve(l, mid - 1), solve(mid, r));
}

int main(){
    int n; cin >> n;
    for(int i = 0; i < n; i++){
        int x; scanf("%d", &x);
        vet.push_back(x);
    }

    cout << solve(0, n-1) << endl;
}