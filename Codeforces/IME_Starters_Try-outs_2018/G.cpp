#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2*acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'
#define eb emplace_back

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

ll fastExpo(ll a, ll b){
    ll res = 1;
    while(b > 0){
        if(b & 1){
            res = (res * a) % MOD;
        }
        b >>= 1;
        a = (a * a) % MOD;
    }
    return res;
}

ll fat(ll n){
    ll res = 1;
    for(int i = 2; i <= n; i++){
        res = (res * i) % MOD;
    }
    return res;
}

ll inv(ll a){
    ll b = MOD - 2;
    ll res = 1;
    while(b){
        if(b & 1){
            res = (res * a) % MOD;
        }
        b >>= 1;
        a = (a * a) % MOD;
    }
    return res;
}

ll combinacao(ll n, ll k){
    return (fat(n) * inv((fat(k) * fat(n - k)) % MOD)) % MOD;
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);

    ll n, k;
    cin >> k >> n;

    cout << ((fastExpo(6, k)%MOD * fat(k)%MOD)%MOD * combinacao(n - 2 * k, k) % MOD) % MOD << endl;

	return 0;
}
