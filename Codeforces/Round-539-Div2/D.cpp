#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int paly(string ss){
    int tam = ss.size();
    for(int i = 0; i < tam; i++){
        if(ss[i] != ss[tam - i - 1]) return 0;
    }
    return 1;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    string s; cin >> s;
    int tam = s.size();
    for(int i = 0; i < tam - 1; i++){
        string first = s.substr(i + 1, tam);
        string second = s.substr(0, i + 1);
        string ss = first + second;
        if(paly(ss) and ss != s){
            cout << 1 << endl;
            return 0;
        }
    }
    for(int i = 0; i < tam/2; i++){
        string first = s.substr(tam - i - 1, i + 1);
        string third = s.substr(i + 1, tam - 2 * (i + 1));
        string second = s.substr(0, i + 1);
        string ss = first + third + second;
        if(paly(ss) and ss != s){
            cout << 2 << endl;
            return 0;
        }
    }
    cout << "Impossible" << endl;

    return 0;
}