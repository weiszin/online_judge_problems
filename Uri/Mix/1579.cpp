#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef tuple<int,int,int> iii;
typedef tuple<int,int,int,int> i4;
typedef pair<ll,ll> pll;

ll n, c, frete;
ll pref[105];
ll vet[105];
ll dp[105][105][15];

ll solve(ll pos, ll pos_ant, ll caminhon){
    if(pos > n) return (pos == pos_ant ? 0 : 1e9);
    if(caminhon <= 0) return 1e9;
    if(dp[pos][pos_ant][caminhon] != -1) return dp[pos][pos_ant][caminhon];
    ll r1 = solve(pos + 1, pos_ant, caminhon);
    ll r2 = max(pref[pos] - pref[pos_ant - 1], solve(pos + 1, pos + 1, caminhon - 1));
    if(r1 == 1e9) return dp[pos][pos_ant][caminhon] = r2;
    if(r2 == 1e9) return dp[pos][pos_ant][caminhon] = r1;
    return dp[pos][pos_ant][caminhon] = min(r1, r2);
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    ll t; cin >> t;
    while(t--){
        cin >> n >> c >> frete;
        memset(pref, 0, sizeof pref);
        for(ll i = 1; i <= n; i++){
            cin >> vet[i];
            pref[i] = pref[i - 1] + vet[i];
        }
        memset(dp, -1, sizeof dp);
        ll res = solve(1, 1, c);
        cout << res << " $" << c * frete * res << endl;
    }
    
    return 0;
}