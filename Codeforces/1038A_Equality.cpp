#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int cnt[30];

int main(int argc, char const *argv[]){

    int n, m;
    scanf("%d %d", &n, &m);
    string s;
    cin >> s;
    for(char c : s){
        cnt[c - 'A']++;
    }
    int mini = cnt[0];
    for(int i = 0; i < m; i++){
        mini = min(mini, cnt[i]);
    }
    printf("%d\n", mini * m);
    return 0;
}
