#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef tuple<int,int,int> i3;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int m;
    while(cin >> m){
        int vet[20 * m + 5];
        for(int i = 0; i < 10 * m; i++){
            cin >> vet[i];
            vet[i+10*m] = vet[i];
        }
        ll res = 0;
        ll acc = 0;
        ll cont = 0;
        for(int i = 0; i < 20*m; i++){
            if(acc + vet[i] <= 0){
                acc = 0;
                cont = 0;
            } else {
                res = max(res, vet[i]+acc);
                cont++;
                if(cont == 10 * m){
                    cont = 0;
                    acc = 0;
                } else {
                    acc += vet[i];
                }
            }
        }
        cout << res << endl;
    }

    return 0;
}