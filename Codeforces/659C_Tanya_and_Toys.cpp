#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

map<int, int> mapa;

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
	
	int n, k;
	cin >> n >> k;
	vi v;
	for(int i = 0; i < n; i++){
		int x;
		cin >> x;
		mapa[x] = 1;
	}
	vi res;
	for(int i = 1; k > 0; i++){
		if(mapa.count(i) != 0) continue;
		if(i > k) break;
		res.push_back(i); k -= i;
	}
	cout << res.size() << endl;
	for(int u : res) cout << u << " ";
	cout << endl;

    return 0;
}
