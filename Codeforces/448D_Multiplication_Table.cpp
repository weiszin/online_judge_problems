#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

int main(){
    ll n, m, k; cin >> n >> m >> k;

    ll l = 0, r = n * m;
    int cnt = 200;
    while(cnt--){
        ll mid = (r + l) / 2;
        ll aux = 0;
        for(ll i = 1; i <= n; i++) aux += min((mid - 1) / i, m);
        if(aux >= k) r = mid - 1;
        else l = mid + 1;
    }
    cout << (l - 1) << endl;
    
    return 0;
}