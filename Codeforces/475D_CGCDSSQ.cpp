#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAX 100000
#define mp make_pair
#define pb push_back
#define FOR(i, s, n) for(int i = s; i < n; i++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define prtl(x) cout << x << endl;
#define prt(x) cout << x

typedef pair<int,int> i2;
typedef vector<int> vi;
typedef vector<i2> vi2;
typedef vector<vi> vvi;
typedef vector<vector<i2> > vvi2;
typedef long long ll;

ll st[100005][30];

ll n;
void pre(ll *vet){
    for(ll i = 0; i < n; i++){
        st[i][0] = vet[i];
    }
    for(ll j = 1; j <= 29; j++){
        for(ll i = 0; i + (1 << j) <= n; i++){
            st[i][j] = __gcd(st[i][j - 1], st[i + (1 << (j - 1))][j - 1]);
        }
    }
}

ll query(ll l, ll r){
    ll diff = (r - l + 1);
    ll j = (ll)log2(diff);
    return __gcd(st[l][j], st[r - (1 << j) + 1][j]);
}

map<ll, ll> conta;

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    cin >> n;
    ll vet[n + 1];
    for(ll i = 0; i < n; i++){
        cin >> vet[i];
    }
    pre(vet);
    // for(ll i = 0; i < n; i++){
    //     for(ll j = i; j < n; j++){
    //         cout << i << " - " << j << ": " << query(i, j) << endl;
    //     }
    // }
    for(ll L = 0; L < n; L++){
        ll R = L;
        while(R < n){
            ll daVez = query(L, R);
            ll l = R, r = n - 1, mid;
            ll res = R;
            while(l <= r){
                mid = (l + r) / 2;
                if(query(R, mid) == daVez){
                    res = mid;
                    l = mid + 1;
                } else {
                    r = mid - 1;
                }
            }
            conta[daVez] += (res - R + 1);
            R = res + 1;
        }
    }
    ll q;
    cin >> q;
    while(q--){
        ll x;
        cin >> x;
        cout << conta[x] << endl;
    }
    return 0;
}
