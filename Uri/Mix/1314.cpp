#include <bits/stdc++.h>

using namespace std;

int n, m, q;
vector<vector<int> > adj;
int visited[10005];
int parent[10005];
int disc[10005], low[10005], parent_2[10005];

int find_pai(int u){
    return (u == parent[u] ? u : parent[u] = find_pai(parent[u]));
}
void union_f(int u, int v){
    parent[u] = v;
}

void DFS(int u){
    static int time = 0;

    visited[u] = 1;
    disc[u] = low[u] = ++time;

    for(int v : adj[u]){
        if(!visited[v]){
            parent_2[v] = u;
            DFS(v);

            low[u] = min(low[u], low[v]);

            if(low[v] > disc[u]){
                // cout << u << " " << v << endl;
                // cout << "Antes: " << find_pai(u) << " " << find_pai(v) << endl;
                union_f(find_pai(u), find_pai(v));
                // cout << "Depois: " << find_pai(u) << " " << find_pai(v) << endl << endl;
            }
        } else if(v != parent_2[u]){
            low[u] = min(low[u], disc[v]);
        }
    }
}

int main(){
    while(cin >> n >> m >> q and (n | m | q)){
        adj.assign(n + 5, vector<int>());
        memset(parent_2, -1, sizeof parent_2);
        memset(low, 0, sizeof low);
        memset(disc, 0, sizeof disc);
        memset(visited, 0, sizeof visited);
        for(int i = 0; i < m; i++){
            int x, y; cin >> x >> y; x--; y--;
            adj[x].push_back(y);
            adj[y].push_back(x);
        }
        for(int i = 0; i < n; i++) parent[i] = i;
        for(int i = 0; i < n; i++){
            if(!visited[i]) DFS(i);
        }
        for(int i = 0; i < q; i++){
            int x, y; cin >> x >> y;
            x--; y--;
            if(find_pai(x) == find_pai(y)) cout << "Y" << endl;
            else cout << "N" << endl;
        }
        cout << "-" << endl;
    }
}
