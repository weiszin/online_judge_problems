#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

int pai[100005];
int n, m;
vector<vector<pair<int,int> > > adj;

int Dijkstra(){
    priority_queue<pair<ll,ll>, vector<pair<ll,ll> >, greater<pair<ll, ll> > > fila;
    ll dist[100005];
    // memset(dist, INF, sizeof dist);
    for(int i = 0; i <= n; i++) dist[i] = 1e17;
    dist[1] = 0;
    pai[1] = 1;
    fila.push({0, 1});
    while(!fila.empty()){
        pair<ll,ll> aux = fila.top(); fila.pop();
        ll u = aux.second;
        ll c = aux.first;
        if(u == n) return 1;
        for(pair<int,int> p : adj[u]){
            ll v = p.first;
            ll w = p.second;
            if(dist[v] > c + w){
                dist[v] = c + w;
                pai[v] = u;
                fila.push({c + w, v});
            }
        }
    }
    return 0;
}

int main(){
    cin >> n >> m;
    adj.assign(n + 5, vector<pair<int,int> >());
    for(int i = 0; i < m; i++){
        int x, y, z; cin >> x >> y >> z;
        adj[x].push_back({y, z});
        adj[y].push_back({x, z});
    }
    if(Dijkstra()){
        vector<int> path;
        int daVez = n;
        while(pai[daVez] != daVez){
            path.push_back(daVez);
            daVez = pai[daVez];
        }
        path.push_back(1);
        reverse(path.begin(), path.end());
        for(int u : path) cout << u << " ";
        cout << endl;
    } else {
        cout << -1 << endl;
    }
    
    return 0;
}