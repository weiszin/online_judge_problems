#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

int vet[50005];
int pref[50005];
int suff[50005];

int main(){
    int n; cin >> n;
    for(int i = 1; i <= n; i++){
        cin >> vet[i];
        pref[i] = pref[i - 1] + vet[i];
    }
    for(int i = n; i >= 1; i--){
        suff[i] = suff[i + 1] + vet[i];
    }
    
    return 0;
}