//https://math.stackexchange.com/questions/1347237/what-is-the-maximum-value-of-the-lcm-of-three-numbers-leq-n-as-a-function-of

#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min (a,b) ((a) < (b) ? (a) : (b))
#define max (a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

int main(){
    ll x; cin >> x;
    if(x == 1) cout << 1 << endl;
    else if(x == 2) cout << 2 << endl;
    else {
        if(x & 1) cout << x * (x - 1) * (x - 2) << endl;
        else {
            if(x % 3 == 0){
                cout << (x - 1) * (x - 2) * (x - 3) << endl;
            } else {
                cout << x * (x - 1) * (x - 3) << endl;
            }
        }
    }
    
    return 0;
}