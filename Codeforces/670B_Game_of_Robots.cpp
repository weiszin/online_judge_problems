#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);

    ll n, m;
    cin >> n >> m;
    vector<ll> vet;
    int f = 0;
    ll res = 0;
    for(int i = 0; i < n; i++){
        ll x;
        cin >> x;
        vet.pb(x);
        res += (i + 1);
        if(!f){
            if(res >= m){
                ll dif = res - m;
                cout << vet[i - dif] << endl;
                f = 1;
            }
        }
    }

    return 0;
}
