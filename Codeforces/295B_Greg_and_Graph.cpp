#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
ll rev[505], dist[505][505], dp[505][505], vivo[505];

//1321441 1030477 698557 345837 121146 0  <-----

int main(){
    cout.sync_with_stdio(0);
    cin.tie(0);

    ll n; cin >> n;
    for(ll i = 1; i <= n; i++)
        for(ll j = 1; j <= n; j++)
            cin >> dp[i][j];
    for(ll i = 1; i <= n; i++)
        cin >> rev[i];
    for(ll i = 1; i <= n; i++)
        dp[i][i] = 0;
    vector<ll> res;
    for(ll k = n; k >= 1; k--){
        ll x = rev[k], sum = 0;
        vivo[x] = 1;
        for(ll i = 1; i <= n; i++)
            for(ll j = 1; j <= n; j++)
                dp[i][j] = min(dp[i][j], dp[i][x] + dp[x][j]);
        for(ll i = 1; i <= n; i++)
            for(ll j = 1; j <= n; j++)
                if(vivo[i] and vivo[j])
                    sum += dp[i][j];
        res.push_back(sum);
    }

    reverse(res.begin(), res.end());
    for(ll val : res)
        cout << val << " ";
    cout << endl;

	return 0;
}