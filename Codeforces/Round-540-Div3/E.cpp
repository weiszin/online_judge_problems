#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    ll n, k; cin >> n >> k;
    if(k*(k-1) < n) cout << "NO" << endl;
    else{
        cout << "YES" << endl;
        int cnt = 0;
        for(int i = 1; i <= k and cnt < n; i++)
            for(int j = i+1; j <= k and cnt < n; j++){
                cout << i << " " << j << endl;
                cnt++;
                if(cnt < n){
                    cout << j << " " <<  i << endl;
                    cnt++;
                }
            }
    }
    return 0;
}