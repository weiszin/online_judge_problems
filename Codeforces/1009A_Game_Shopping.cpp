#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);

    int n, m;
    cin >> n >> m;
    int v1[n + 1], v2[m + 1];
    for(int i = 0; i < n; i++){
        cin >> v1[i];
    }
    for(int j = 0; j < m; j++){
        cin >> v2[j];
    }

    int l = 0, r = 0;
    int res = 0;
    while(l < n and r < m){
        if(v1[l] <= v2[r]){
            res++;
            r++;
        }
        l++;
    }
    cout << res << endl;

    return 0;
}
