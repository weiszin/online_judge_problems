#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;

long long mod = 1e9+7;
char s[100005];
vi adj[100005];
int vis[100005];
set<int> S;

void solve(int u){
    vis[u] = 1;
    if(s[u] != '*') S.insert(s[u]);
    for(int v : adj[u])
        if(!vis[v])
            solve(v);
}

int main(){

    int n, m; scanf("%d %d", &n, &m);
    scanf("%s", s+1);

    for(int i = 1; i <= n/2; i++){
            adj[i].push_back(n-i+1);
            adj[n-i+1].push_back(i);
        }

    for(int i = 0; i < m; i++){
        int u, v; scanf("%d %d", &u, &v);
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    long long res = 1;
    for(int i = 1; i <= n; i++)
        if(!vis[i]){
            S.clear();
            solve(i);
            if(S.size() > 1){
                printf("0\n");
                return 0;
            }
            if(S.size() == 0)
                res = (res*26)%mod;
        }
    printf("%lld\n", res);
	return 0;
}
