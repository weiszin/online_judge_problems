#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

map<char, char> mapa;

int solve(string s){
    int pali = 1, mirror = 1;
    string aux = s;
    reverse(aux.begin(), aux.end());
    if(aux != s) pali = 0;
    string res = "";
    for(int i = 0; i < aux.size(); i++){
        if(mapa.count(aux[i])) res += mapa[aux[i]];
        else res += aux[i], mirror = 0;
    }
    if(res != s) mirror = 0;
    if(!mirror and !pali) return 0;
    if(pali and !mirror) return 1;
    if(!pali and mirror) return 2;
    return 3;
}

int main(){

    mapa['E'] = '3';
    mapa['A'] = 'A';
    mapa['3'] = 'E';
    mapa['J'] = 'L';
    mapa['L'] = 'J';
    mapa['S'] = '2';
    mapa['2'] = 'S';
    mapa['Z'] = '5';
    mapa['5'] = 'Z';
    mapa['H'] = 'H';
    mapa['I'] = 'I';
    mapa['M'] = 'M';
    mapa['O'] = 'O';
    mapa['T'] = 'T';
    mapa['U'] = 'U';
    mapa['V'] = 'V';
    mapa['W'] = 'W';
    mapa['X'] = 'X';
    mapa['Y'] = 'Y';
    mapa['1'] = '1';
    mapa['8'] = '8';

    string s;
    while(cin >> s){
        int res = solve(s);
        cout << s << " -- ";
        switch(res){
            case 0:
                cout << "is not a palindrome." << endl;
                break;
            case 1:
                cout << "is a regular palindrome." << endl;
                break;
            case 2:
                cout << "is a mirrored string." << endl;
                break;
            case 3:
                cout << "is a mirrored palindrome." << endl;
                break;
        }
        cout << endl;
    }

    
    return 0;
}