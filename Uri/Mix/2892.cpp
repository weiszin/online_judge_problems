#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int mmc(int a, int b){return (a * b) / __gcd(a,b);}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int t, a, b;
    while(cin >> t >> a >> b and (t + a + b)){
        int aux = sqrt(t);
        vector<int> res;
        for(int i = 1; i <= aux; i++){
            if(t % i == 0){
                int f = i;
                int s = t / i;
                if(mmc(a, mmc(b, f)) == t) res.push_back(f);
                if(s != f){
                    if(mmc(a, mmc(b, s)) == t) res.push_back(s);
                }
            }
        }
        sort(res.begin(), res.end());
        for(int i = 0; i < res.size(); i++){
            if(i) cout << " ";
            cout << res[i];
        } cout << endl;
    }
    
    return 0;
}