#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

ll freq_s[300], freq_t[300];

int main()
{
    cout.sync_with_stdio(0);
    cin.tie(0);

    string s, t;
    cin >> s >> t;
    ll flag = 0;

    for (char c : s)
    {
        if (c != '?')
            freq_s[c]++;
        else
            flag++;
    }
    for (char c : t)
        freq_t[c]++;

    ll mini = 1e9;
    for (ll i = 'a'; i <= 'z'; i++)
    {
        if (freq_t[i] > 0)
        {
            mini = min(mini, freq_s[i] / freq_t[i]);
        }
    }

    for (ll i = 'a'; i <= 'z'; i++)
        freq_s[i] -= freq_t[i] * mini;

    ll esq = 0, dir = 1e6, res = 0;
    while (esq <= dir)
    {
        ll mid = (esq + dir) / 2;
        ll ok = 1, diff = 0;
        for (ll i = 'a'; i <= 'z'; i++)
            diff += max(freq_t[i] * mid - freq_s[i], 0LL);
        if (diff <= flag)
        {
            esq = mid + 1;
            res = max(res, mid);
        }
        else
            dir = mid - 1;
    }

    ll pos = 0;
    for (ll i = 'a'; i <= 'z'; i++)
    {
        ll aux = freq_s[i] - freq_t[i] * res;
        while (aux++ < 0)
        {
            while (s[pos] != '?')
                pos++;
            s[pos] = i;
        }
    }
    for (ll i = 0; i < s.size(); i++)
        if (s[i] == '?')
            s[i] = 'w';
    cout << s << endl;
    return 0;
}
