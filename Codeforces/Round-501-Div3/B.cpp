#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef long long ll;

typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

int main(){
	ios_base::sync_with_stdio(false);

	int n;
	cin >> n;
	string a, b;
	cin >> a >> b;
	vi vet;
	int ff = 1;
	for(int i = 0;  i < n; i++){
		if(b[i] != a[i]){
			int f = 0;
			for(int j = i + 1; j < n; j++){
				if(a[j] == b[i]){
					f = 1;
					while(j > i){
						swap(a[j - 1], a[j]);
						vet.push_back(j);
						j--;
					}
					break;
				}
			}
			if(!f) ff = 0;
		}
	}
	if(!ff) cout << "-1" << endl;
	else {
		cout << vet.size() << endl;
		for(int u : vet){
			cout << u << " ";
		} cout << endl;
	}

	return 0;
}
