#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef tuple<int,int,int> i3;
typedef pair<ll,ll> pll;

ll mod = 1e9+7;

ll expo(ll b, ll e){
    ll res = 1;
    while(e > 0){
        if(e & 1){
            res = (res * b) % mod;
            e--;
        }
        e /= 2;
        b = (b * b) % mod;
    }
    return res;
}

ll sumOfDigits(ll n){
    if(n < 10){
        return (n * (n + 1)) / 2;
    }
    int d = log10(n);
    ll *a = new ll[d + 1];
    a[0] = 0; a[1] = 45;
    for(int i = 2; i <= d; i++){
        a[i] = (a[i - 1] * 10)%mod + (45 * (ll)ceil(pow(10, i - 1)))%mod;
    }
    ll p = ((ll)ceil(pow(10, d)));
    ll msd = n / p;
    ll first = (msd * a[d]) % mod;
    ll second = ((msd * (msd - 1) / 2) * p)%mod;
    ll third = (msd * (1 + n % p))%mod;
    ll fourth = (sumOfDigits(n % p))%mod;
    return (first + second + third + fourth)%mod;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    ll a, b;
    while(cin >> a >> b){
        cout << (sumOfDigits(b)-sumOfDigits(a-1)+mod)%mod << endl;
    }

    return 0;
}