#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int sumOfDigits(int a){ 
    int sum = 1; 
    while (a) 
    { 
        sum *= a % 10; 
        a /= 10; 
    } 
    return sum; 
} 
  
int findMax(int x){ 
    int b = 1, ans = x; 
    while (x) {
        int cur = (x - 1) * b + (b - 1); 

        if (sumOfDigits(cur) > sumOfDigits(ans) ||  
           (sumOfDigits(cur) == sumOfDigits(ans) &&  
            cur > ans)) 
            ans = cur; 
  
        x /= 10; 
        b *= 10;
    } 
  
    return ans; 
} 

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int n; cin >> n;
    int num_digits = n > 0 ? (int) log10 ((double) n) + 1 : 1;
    if(num_digits == 1){
        cout << n << endl;
    } else {
        int tmp = findMax(n);
        int res = 1;
        while(tmp > 0){
            int u = tmp % 10;
            res *= u;
            tmp /= 10;
        }
        cout << res << endl;

    }
    
    return 0;
}