#include <bits/stdc++.h>
using namespace std;


vector<string> vs;
vector<int> idx[105];

int main(){
    cout.sync_with_stdio(false); cin.tie(nullptr);
    int n, m;
    cin >> n >> m;

    for(int i = 0; i < n; i++){
        string s;
        cin >> s;
        vs.push_back(s);
    }
    int f = 1;
    for(int i = 0; i < n; i++){
        for(int j = 0; j < m; j++)
            if(vs[i][j] == '1'){
                idx[i].push_back(j-m);
                idx[i].push_back(j);
                idx[i].push_back(j+m);
            }
        sort(idx[i].begin(), idx[i].end());
        if(idx[i].empty()) f = 0;
    }
    if(!f) cout << "-1" << endl;
    else {
        int res = 1e9;
        for(int j = 0; j < m; j++){
            int aux = 0;
            //cout << "selecionando coluna " << j << endl;
            for(int i = 0; i < n; i++){
                int posR = upper_bound(idx[i].begin(),idx[i].end(),j)-idx[i].begin();
                int posL = lower_bound(idx[i].begin(), idx[i].end(), j)-idx[i].begin();
                if(posR == posL) posL--;
                posR = idx[i][posR];

                posL = idx[i][posL];
                //cout << "linha " << i << " com distancia " << posL << " " << posR << endl;
                aux += min(abs(j-posL), posR - j);
            }
            res = min(res, aux);
        }
        cout << res << endl;
    }
    return 0;
}
