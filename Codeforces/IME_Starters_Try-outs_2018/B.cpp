#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2*acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'
#define eb emplace_back
#define pb push_back

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

int start, n;

vector<string> nomes;
vector<int> custos;
vector<int> moc;
int V[1010][2020];

void pegaItens(int cap, int n, vector<int> &c){
	moc.clear();
	while(cap > 0 and n >= 1){
		if(V[n - 1][cap] == V[n][cap]){
			n--;
		} else {
			moc.pb(n - 1);
			cap -= c[n - 1];
			n--;
		}
	}
}

int mochila(int peso, vector<int> &c, int n){
	for(int i = 0; i <= n; i++){
		for(int j = 0; j <= 2000; j++){
			if(i == 0 or j == 0){
				V[i][j] = 0;
			} else if(c[i - 1] <= j){
				V[i][j] = max(V[i - 1][j], V[i - 1][j - c[i - 1]] + c[i - 1]);
			} else {
				V[i][j] = V[i - 1][j];
			}
		}
	}

	int mid = (peso & 1 ? (peso + 1) / 2 : peso / 2);
	mid -= start;
	int pos = -1;
	for(int i = mid; i <= 2000; i++){
		if(V[n][i] >= mid) pos = i;
		if(pos != -1) break;
	}

	if(pos == -1) return pos;
	else {
		pegaItens(pos, n, c);
		return 1;
	}
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);

	cin >> start >> n;
	for(int i = 0; i < n; i++){
		string s;
		int p;
		cin >> s >> p;
		nomes.pb(s);
		custos.pb(p);
	}

	int k;
	cin >> k;
	for(int i = 0; i < k; i++){
		int peso;
		cin >> peso;
		int r = mochila(peso, custos, n);
		if(r <= 0) cout << r << endl;
		else {
			cout << moc.size() << " ";
			for(int u : moc){
				cout << nomes[u] << " ";
			} cout << endl;
		}
	}

	return 0;
}
