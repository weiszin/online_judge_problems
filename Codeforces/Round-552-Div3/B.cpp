#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n; cin >> n;
    int vet[n + 5];
    set<int> s;
    for(int i = 0; i < n; i++){
        cin >> vet[i];
        s.insert(vet[i]);
    }
    int res = 1e9;
    for(int u = 1; u <= 100; u++){
        int flag = 1;
        int d = 0;
        for(int i = 0; i < n; i++){
            int aux = abs(vet[i] - u);
            if(aux != 0 and !d){
                d = aux;
            } else if(aux != 0 and d != aux){
                flag = 0;
            }
        }
        if(flag){
            res = min(res, d);
        }
    }

    if(res >= 1e6) cout << -1 << endl;
    else cout << res << endl;

    return 0;
}