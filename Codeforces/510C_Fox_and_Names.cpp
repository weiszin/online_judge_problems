#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

vector<vector<int> > adj(305);

int cycle(int u, int *vis){
    // cout << (char)u << " -> ";
    vis[u] = 1;
    for(int v : adj[u]){
        if(vis[v] == 1) return 1;
        if(!vis[v]){
            if(cycle(v, vis)) return 1;
        }
    }
    vis[u] = 2;
    return 0;
}

void topo(int u, int *vis, stack<int> &p){
    vis[u] = 1;
    for(int v : adj[u]){
        if(!vis[v]) topo(v, vis, p);
    }
    p.push(u);
}

void inverte(stack<int> &p){
    stack<int> aux;
    while(!p.empty()){
        aux.push(p.top());
        p.pop();
    }
    p = aux;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n; cin >> n;
    vector<string> vs;
    for(int i = 0; i < n; i++){
        string s; cin >> s;
        vs.push_back(s);
    }
    int vis[300];
    memset(vis, 0, sizeof vis);

    for(int i = 0; i < 100; i++){
        for(int j = 1; j < n; j++){
            if(vis[j - 1]) continue;
            if(vs[j - 1].size() > i and vs[j].size() > i and vs[j - 1][i] != vs[j][i]){
                vis[j - 1] = 1;
                adj[vs[j][i]].push_back(vs[j - 1][i]);
                // cout << (char)vs[j][i] << " -> " << (char)vs[j - 1][i] << endl;
            }
        }
    }
    
    memset(vis, 0, sizeof vis);
    int impo = 0;
    for(char c = 'a'; c <= 'z' and !impo; c++){
        if(vis[c] == 0){
            impo |= cycle(c, vis);
        }
    }
    for(int i = 1; i < n and !impo; i++){
        if(vs[i].size() < vs[i - 1].size()){
            string aux = vs[i - 1].substr(0, vs[i].size());
            if(aux == vs[i]) impo = 1;
        }
    }

    if(impo) cout << "Impossible" << endl;
    else {
        memset(vis, 0, sizeof vis);
        for(char c = 'a'; c <= 'z'; c++){
            if(!vis[c]){
                stack<int> p;
                topo(c, vis, p);
                inverte(p);
                while(!p.empty()){
                    cout << (char)p.top();
                    p.pop();
                }
            }
        } cout << endl;
    }
    
    return 0;
}