#include <bits/stdc++.h>
using namespace std;

map<string,int> mapa;
map<string,int> maparev;

int main(){

    cout.sync_with_stdio(0);
    cin.tie(0);
    vector<string> vs;
    int n; cin >> n;
    for(int i = 0; i < n; i++){
        string s; cin >> s;
        vs.push_back(s);
        int len = s.size();
        set<string> S;
        for(int j = 1; j <= len; j++)
            for(int k = 0; k <= len - j; k++){
                S.insert(s.substr(k, j));
            }
        for(string str: S){
            mapa[str]++;
            maparev[str] = i;
        }
    }
    int m; cin >> m;
    while(m--){
        string s; cin >> s;
        if(maparev.count(s))
            cout << mapa[s] << " " << vs[ maparev[s] ] << endl;
        else
            cout << "0 -" << endl;
    }
	return 0;
}
