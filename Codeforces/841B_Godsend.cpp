#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define SWP(a,b) ((a) ^= (b) ^= (a) ^= (b))
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);
    int n;
    cin >> n;
    ll sum = 0;
    int imp = 0;
    for(int i = 0; i < n; i++){
        int x; cin >> x;
        if(x & 1) imp = 1;
        sum += x;
    }
    if(sum & 1) cout << "First" << endl;
    else {
        if(imp) cout << "First" << endl;
        else cout << "Second" << endl;
    }

    return 0;
}
