#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min (a,b) ((a) < (b) ? (a) : (b))
#define max (a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

struct Node {
    Node *l, *r;
    int key, prior, tam, rev;

    Node(int _key){
        key = _key;
        prior = rand();
        tam = 0;
        rev = 0;
        l = r = NULL;
    }
};

Node *rt;

void updateRev(Node *&root){
    if(!root or !(root -> rev)) return;
    swap(root -> l, root -> r);
    if(root -> l) root -> l -> rev ^= 1;
    if(root -> r) root -> r -> rev ^= 1;
    root -> rev ^= 1;
}

void updateTam(Node *&root){
    if(!root) return;
    root -> tam = 0;
    if(root -> l) root -> tam += root -> l -> tam + 1;
    if(root -> r) root -> tam += root -> r -> tam + 1;
}

pair<Node*, Node*> split(Node *root, int idx, int add){
    if(!root) return {NULL, NULL};
    updateRev(root);
    int tamL = (root -> l ? root -> l -> tam + 1: 0);
    if(tamL + add < idx){
        pair<Node*, Node*> aux = split(root -> r, idx, add + tamL + 1);
        root -> r = aux.first;
        updateTam(root), updateTam(aux.second);
        return {root, aux.second};
    } else {
        pair<Node*, Node*> aux = split(root -> l, idx, add);
        root -> l = aux.second;
        updateTam(root), updateTam(aux.first);
        return {aux.first, root};
    }
}

Node* merge(Node *&l, Node *&r){
    updateRev(l); updateRev(r);
    if(!l) return r;
    if(!r) return l;
    if(l -> prior > r -> prior){
        auto aux = merge(l -> r, r);
        l -> r = aux;
        updateTam(l);
        return l;
    } else {
        auto aux = merge(l, r -> l);
        r -> l = aux;
        updateTam(r);
        return r;
    }
}

void insert(Node *&root, Node *novo, int idx){
    pair<Node*, Node*> aux = split(root, idx, 0);
    Node* tmp = merge(aux.first, novo);
    root = merge(tmp, aux.second);
}

void printTree(Node *root){
    if(root == NULL) return;
    updateRev(root);
    printTree(root -> l);
    cout << root -> key << " ";
    printTree(root -> r);
}

void reverseSeg(Node *&root, int l, int r){
    pair<Node*, Node*> aux = split(root, l, 0);
    // printTree(aux.first);
    // cout << endl;
    pair<Node*, Node*> aux2 = split(aux.second, r+1, (aux.first ? aux.first -> tam + 1 : 0));
    // printTree(aux2.first);
    aux2.first -> rev ^= 1;
    Node* tmp = merge(aux2.first, aux2.second);
    root = merge(aux.first, tmp);
}

void shiftSeg(Node *&root, int l, int r){
    pair<Node*, Node*> aux = split(root, l, 0);
    pair<Node*, Node*> aux2 = split(aux.second, r + 1, (aux.first ? aux.first -> tam + 1 : 0));
    pair<Node*, Node*> aux3 = split(aux2.first, r, (aux.first ? aux.first -> tam + 1 : 0));
    Node* r1 = merge(aux3.second, aux3.first);
    Node *r2 = merge(r1, aux2.second);
    root = merge(aux.first, r2);
}

int query(Node *root, int idx, int add){
    if(!root) return 0;
    updateRev(root);
    int tamL = (root -> l ? root -> l -> tam + 1 : 0);
    if(tamL + add == idx) return root -> key;
    if(tamL + add < idx){
        return query(root -> r, idx, tamL + add + 1);
    } else {
        return query(root -> l, idx, add);
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    srand(time(NULL));
    rt = NULL;

    int n, q, m;
    cin >> n >> q >> m;
    for(int i = 0; i < n; i++){
        int x; cin >> x;
        insert(rt, new Node(x), i);
    }
    // printTree(rt);
    // cout << endl;
    for(int i = 0; i < q; i++){
        int t, a, b; cin >> t >> a >> b;
        a--; b--; 
        if(t == 2){
            reverseSeg(rt, a, b);
        } else {
            shiftSeg(rt, a, b);
        }
    }
    for(int i = 0; i < m; i++){
        int x; cin >> x;
        x--;
        cout << query(rt, x, 0) << " ";
    } cout << endl;
    return 0;
}
