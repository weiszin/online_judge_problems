#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define SWP(a,b) ((a) ^= (b) ^= (a) ^= (b))
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int n, k;

ll dp[2010][2010];

int solve(int ant, int tam){
    if(tam == k) return 1;
    ll &dpzin = dp[ant][tam];
    if(dpzin != -1) return dpzin;
    ll res = 0;
    for(int i = ant; i <= n; i += ant){
        res = (res % MOD + solve(i, tam + 1) % MOD) % MOD;
    }
    return dpzin = res;
}

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);

    cin >> n >> k;

    ll res = 0;
    memset(dp, -1, sizeof dp);
    for(int i = 1; i <= n; i++){
        res = (res + solve(i, 1)) % MOD;
    }
    cout << res << endl;

    return 0;
}
