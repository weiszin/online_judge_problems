#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<ll,ll> ii;
typedef pair<ll,ll> pll;

ll v[200005];

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    ll n, m; cin >> n >> m;
    for(ll i = 0; i <  n; i++)
        cin >> v[i];
    sort(v, v+n);
    reverse(v, v+n);
    ll esq = 1, dir = n, res = 1e9;
    while(esq <= dir){
        ll mid = (esq+dir)/2;
        ll sum = 0;
        for(ll i = 0; i < n; i++)
            sum += max(0, v[i]-i/mid);
        if(sum >= m){
            dir = mid-1;
            res = min(res, mid);
        }
        else
            esq = mid+1;
    }

    if(res == 1e9) cout << -1 << endl;
    else cout << res << endl;
    


    return 0;
}