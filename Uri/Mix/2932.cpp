#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef tuple<int,int,int> i3;
typedef pair<ll,ll> pll;

vector<vector<ii> > adj;
int doentes_a[200005];
int pai[200005];

void union_f(int u, int v){
    pai[u] = v;
}
int find_pai(int u){
    if(pai[u] == u) return u;
    return pai[u] = find_pai(pai[u]);
}


int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n, m; cin >> n >> m;

    adj.assign(n + 5, vector<ii>());

    for(int i = 0; i < m; i++){
        int x; cin >> x;
        doentes_a[x] = 1;
    }
    vector<i3> vet;
    for(int i = 0; i < n - 1; i++){
        int u, v, w; cin >> u >> v >> w;
        vet.push_back(i3(w, u, v));
        // adj[u].push_back(ii(v, w));
        // adj[v].push_back(ii(u, w));
    }
    for(int i = 0; i <= n; i++) pai[i] = i;
    sort(vet.rbegin(), vet.rend());

    ll res = 0;

    for(i3 p : vet){
        int w, u, v; tie(w, u, v) = p;
        // cout << "Tentando ligar: " << u << " " << v << endl;
        int up = find_pai(u), vp = find_pai(v);
        if(doentes_a[up] and doentes_a[vp]){
            // cout << "Deu ruim!" << endl;
            res += w;
        } else {
            // cout << "Deu boa!" << endl;
            if(doentes_a[up]){
                union_f(vp, up);
                // cout << "Unindo: pai[" << vp << "] = " << up << endl;
            } else {
                union_f(up, vp);
                // cout << "Unindo: pai[" << up << "] = " << vp << endl;
            }
        }
    }
    cout << res << endl;

    return 0;
}