#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

int vet[2005], cnt[2005], vis[2005];

int main()
{

    int n, t, r;
    scanf("%d %d %d", &n, &t, &r);
    for (int i = 1; i <= n; i++)
    {
        scanf("%d", &vet[i]);
        vet[i] += 300;
    }

    if (t + 1 <= r)
    {
        printf("-1\n");
        return 0;
    }

    // for(int i = vet[1] - r; i <= vet[1]; i++){
    //     for(int j = 0; j < t; j++){
    //         cnt
    //     }
    // }

    // 1 1 1 1 1 1 P1 1 1 1 1 1 1 P2

    int res = 0;
    for (int i = 1; i <= n; i++)
    {
        if (cnt[vet[i]] >= r)
            continue;
        int aux = r - cnt[vet[i]];
        res += aux;
        for (int j = vet[i]; aux > 0; j--, aux--)
        {
            if (vis[j])
            {
                printf("-1\n");
                return 0;
            }
            // printf("ligando em %d\n", j-300);
            vis[j] = 1;
            for (int k = 0; k < t; k++)
                cnt[j + k]++;
        }
    }
    printf("%d\n", res);

    return 0;
}