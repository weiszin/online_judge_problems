#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);

    int n;
    cin >> n;
    map<int, vi> mapa;
    for(int i = 0; i < n; i++){
        int x; cin >> x;
        mapa[x].pb(i);
    }
    map<int, vi>::iterator it = mapa.begin();
    int res = INF;
    for(int i = 1; i < it -> second.size(); i++){
        int dif = it -> second[i] - it -> second[i - 1];
        if(dif < res) res = dif;
    }
    cout << res << endl;

    return 0;
}
