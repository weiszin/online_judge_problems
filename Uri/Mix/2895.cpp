#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int dp[12][9000];

int solve(int pos, int diff){
    if(diff == 0) return 0;
    if(pos >= 12) return 1e9;
    if(dp[pos][diff + 4100] != -1) return dp[pos][diff + 4100];
    int res = solve(pos + 1, diff);
    res = min(res, solve(pos + 1, diff + (1 << pos)) + 1);
    res = min(res, solve(pos + 1, diff - (1 << pos)) + 1);
    return dp[pos][diff + 4100] = res;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int t; cin >> t;
    int n;
    memset(dp, -1, sizeof dp);
    while(t--){
        cin >> n;
        cout << solve(0, -n) << endl;
    }
    
    return 0;
}