#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'
#define eb emplace_back

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

map<string, string> mapa;

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
	
	int a, b, c, d;
	cin >> a >> b >> c >> d;
	ll r = 0;
	if(a == c) r = abs(d - b);
	else if(b == d) r = abs(a - c);
	else {
		ll f = abs(a - c);
		ll s = abs(b - d);
		if(f < s){
			r = s;
		} else {
			r = f;	
		}
	}

	cout << r << endl;
	return 0;
}
