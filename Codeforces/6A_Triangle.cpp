#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define SWP(a,b) ((a) ^= (b) ^= (a) ^= (b))
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int solve(int *vet){
    int a, b, c;
    int t = 0, d = 0;
    //First
    a = vet[0], b = vet[1], c = vet[2];
    if(a + b > c and a + c > b and b + c > a) t = 1;
    else {
        int aux[] = {a, b, c};
        sort(aux, aux + 3);
        if(aux[0] + aux[1] == aux[2]) d = 1;
    }

    //Second
    a = vet[0], b = vet[1], c = vet[3];
    if(a + b > c and a + c > b and b + c > a) t = 1;
    else {
        int aux[] = {a, b, c};
        sort(aux, aux + 3);
        if(aux[0] + aux[1] == aux[2]) d = 1;
    }

    //Third
    a = vet[1], b = vet[2], c = vet[3];
    if(a + b > c and a + c > b and b + c > a) t = 1;
    else {
        int aux[] = {a, b, c};
        sort(aux, aux + 3);
        if(aux[0] + aux[1] == aux[2]) d = 1;
    }

    // Forty
    a = vet[0], b = vet[2], c = vet[3];
    if(a + b > c and a + c > b and b + c > a) t = 1;
    else {
        int aux[] = {a, b, c};
        sort(aux, aux + 3);
        if(aux[0] + aux[1] == aux[2]) d = 1;
    }

    if(t) return 1;
    else if(d) return 0;
    return -1;
}

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);
    int vet[4];
    for(int i = 0; i < 4; i++){
        cin >> vet[i];
    }
    int s = solve(vet);
    if(s == 0){
        cout << "SEGMENT" << endl;
    } else if(s == 1) cout << "TRIANGLE" << endl;
    else cout << "IMPOSSIBLE" << endl;

    return 0;
}
