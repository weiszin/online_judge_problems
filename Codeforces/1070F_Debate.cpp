#include <bits/stdc++.h>
using namespace std;

int main()
{

    int n;
    cin >> n;
    vector<vector<int>> vet(4);
    for (int i = 0; i < n; i++)
    {
        string s;
        cin >> s;
        int x;
        cin >> x;
        if (s == "00")
            vet[0].push_back(x);
        if (s == "01")
            vet[1].push_back(x);
        if (s == "10")
            vet[2].push_back(x);
        if (s == "11")
            vet[3].push_back(x);
    }
    for (int i = 1; i < 4; i++)
    {
        sort(vet[i].rbegin(), vet[i].rend());
    }
    int l = 0, r = 0;
    int res = 0;
    while (l < vet[1].size() and r < vet[2].size())
    {
        res += vet[1][l] + vet[2][r];
        l++;
        r++;
    }
    while (l < vet[1].size())
        vet[0].push_back(vet[1][l++]);
    while (r < vet[2].size())
        vet[0].push_back(vet[2][r++]);
    sort(vet[0].rbegin(), vet[0].rend());
    l = 0;
    for (int i = 0; i < vet[3].size(); i++)
    {
        res += vet[3][i];
        if (l < vet[0].size())
            res += vet[0][l++];
    }
    cout << res << endl;

    return 0;
}
