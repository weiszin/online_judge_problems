#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int isVowel(char c){
    return (c == 'a' or c == 'e' or c == 'i' or c == 'o' or c == 'u' or c == 'y');
}

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);

    int n;
    cin >> n;
    vector<char> vet;
    for(int i = 0; i < n; i++){
        char c;
        cin >> c;
        vet.push_back(c);
    }
    for(int i = 0; i < vet.size() - 1; i++){
        if(isVowel(vet[i]) and isVowel(vet[i + 1])){
            vet.erase(vet.begin() + i + 1);
            i--;
        }
    }
    for(char c : vet){
        cout << c;
    } cout << endl;

    return 0;
}
