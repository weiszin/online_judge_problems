#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

ll v[1005];

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    ll n; cin >> n;
    ll k = 1;
    for(ll i = 0; i < n; i++){
        cin >> v[i];
        k *= v[i];
    }
    map<ll,ll> cnt;
    ll res = 1;
    for(ll i = 1; i <= k; i++)
        if(k%i == 0){
            vector<ll> vec;
            ll x = i;
            for(ll j = 2; j <= x; j++)
                while(x%j == 0)
                    x /= j, vec.push_back(j);
            if(x != 1) vec.push_back(x);
            for(ll val : vec){
                cout << val << " ";
                cnt[val]++;
            }
            cout << endl;
            res *= i;
        }
    for(auto par : cnt)
        cout << par.first << " " << par.second << endl;
    cout << res << endl;
    return 0;
}