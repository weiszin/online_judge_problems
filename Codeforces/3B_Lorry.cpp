#include <bits/stdc++.h>
using namespace std;

typedef pair<int,int> ii;
typedef long long ll;

vector<ii> adj[5];
ll pref[200005][5];

int main(){
    int n, v; scanf("%d %d", &n, &v);
    for(int i = 0; i < n; i++){
        int x, y; scanf("%d %d", &x, &y);
        adj[x].push_back({y,i});
    }

    sort(adj[1].rbegin(), adj[1].rend());
    sort(adj[2].rbegin(), adj[2].rend());

    ll tot = 0;
    for(int i = 0; i < adj[1].size(); i++)
        tot += adj[1][i].first, pref[i][1] = tot;
    tot = 0;
    for(int i = 0; i < adj[2].size(); i++)
        tot += adj[2][i].first, pref[i][2] = tot;

    // for(int i = 0; i < adj[1].size(); i++)
    //     printf("pref[%d][%d] = %lld\n", i, 1, pref[i][1]);
    // for(int i = 0; i < adj[2].size(); i++)
    //     printf("pref[%d][%d] = %lld\n", i, 2, pref[i][2]);    
    int maxi2 = min(v/2-1, (int)adj[2].size()-1);
    ll res = 0;
    if(maxi2 >= 0) res = pref[maxi2][2];
    int resid = -1;

    for(int i = 0; i < adj[1].size() and i+1 <= v; i++){
        int aux = (v-i-1);
        ll sum = pref[i][1];
        if(aux > 1){
            aux = min(aux/2-1, (int)adj[2].size()-1);
            if(aux >= 0)
                sum += pref[aux][2];
        }
        if(sum > res)
            res = sum, resid = i;
    }
    // yesssssss
    vector<int> idx;
    for(int i = 0; i <= resid; i++)
        idx.push_back(adj[1][i].second);
    int aux = (resid == -1 ? v : v-resid-1);
    aux = min(aux/2-1, (int)adj[2].size()-1);
    for(int i = 0; i <= aux; i++)
        idx.push_back(adj[2][i].second);
    printf("%lld\n", res);
    for(int val : idx)
        printf("%d ", val+1);
    printf("\n");
	return 0;
}
