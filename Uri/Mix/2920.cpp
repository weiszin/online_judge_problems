#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef tuple<int,int,int> i3;
typedef pair<ll,ll> pll;

string dp[105][105];
string s;

int menor(string a, string b){
    if(a.size() < b.size()) return 1;
    else if(b.size() < a.size()) return 0;
    else {
        if(a < b) return 1;
        return 0;
    }
}

string solve(int pos, int k){
    if(pos >= s.size()){
        if(k) return "99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999";
        return "";
    }
    if(k < 0){
        return "99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999";
    }
    if(dp[pos][k] != "-1") return dp[pos][k];
    string aux = s[pos] + solve(pos + 1, k);
    string aux2 = solve(pos + 1, k - 1);
    if(menor(aux, aux2)){
        return dp[pos][k] = aux;
    }
    return dp[pos][k] = aux2;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    while(cin >> s){
        int k; cin >> k;
        for(int i = 0; i < 105; i++)
            for(int j = 0; j < 105; j++)
                dp[i][j] = "-1";
        cout << solve(0, k) << endl;
    }

    return 0;
}