#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int a, b; cin >> a >> b;
    if(a > b) cout << "Eu odeio a professora!" << endl;
    else if(b - a >= 3) cout << "Muito bem! Apresenta antes do Natal!" << endl;
    else {
        cout << "Parece o trabalho do meu filho!" << endl;
        if(b + 2 >= 25) cout << "Fail! Entao eh nataaaaal!" << endl;
        else cout << "TCC Apresentado!" << endl;
    }
    
    return 0;
}