#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);
    int n, m;
    cin >> n >> m;
    vector<string> vet;
    for(int i = 0; i < n; i++){
        string s;
        cin >> s;
        vet.pb(s);
    }
    int res = 0;
    int vis[n + 1];
    memset(vis, 0, sizeof vis);
    for(int i = 0; i < m; i++){
        int maior = 0;
        vi cnt;
        for(int j = 0; j < n; j++){
            int tmp = vet[j][i] - '0';
            if(tmp == maior) cnt.pb(j);
            else if(tmp > maior) maior = tmp, cnt.clear(), cnt.pb(j);
        }
        for(int u : cnt) vis[u] = 1;
    }
    for(int i = 0; i < n; i++) res += vis[i];
    cout << res << endl;

    return 0;
}
