#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define INFL (1LL << 58)
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))
#define GET(x,t) get<x>(t)

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;
typedef vector<ii> vii;
typedef vector<vii> vvii;
typedef tuple<int,int,int> i3;
typedef tuple<ll,ll,ll> ll3;

vector<ll> primos;
bool isPrime[2000005];

void crivo(){
    primos.push_back(2LL);
    memset(isPrime, true, sizeof isPrime);
    for(ll i = 4; i <= 2000000; i += 2){
        isPrime[i] = false;
    }
    for(ll i = 3; i <= 2000000; i += 2){
        if(isPrime[i]){
            primos.push_back(i);
            for(ll j = i * i; j <= 2000000; j += i) isPrime[j] = false;
        }
    }
}

int factNB(ll n, ll b){
    ll aux = n - 1;
    while(n < b and aux > 0){
        n *= aux;
        aux--; 
    }
    return (n <= b);
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);    
    ll n, b;
    while(cin >> n >> b){
        ll j = b;
        ll res = -1;
        crivo();
        for(ll i : primos){
            if(j % i == 0){
                ll p = 0;
                while(j % i == 0){
                    p++;    
                    j /= i;
                }
                ll c = 0;
                ll k = n;
                ll aux = i;
                while(k / i > 0){
                    c += (k / i);
                    k /= i;
                }
                if(res == -1){
                    res = c / p;
                } else {
                    res = min(res, c / p);
                }
            }
        }
        if(res == -1){
            res = n / b;
        }
        if(j > 1){
            ll p = 1;
            ll c = 0;
            ll k = n;
            ll i = j;
            while(k / i > 0){
                c += (k / i);
                k /= i;
            }
            res = min(res, c / p);
        }
        cout << res << endl;
    }
    return 0;
}