#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);

    int n;
    cin >> n;
    vi v;
    for(int i = 0; i < n; i++){
        int x;
        cin >> x;
        v.push_back(x);
    }
    sort(v.begin(), v.end());
    int r = 0;
    while(v.size() > 0){
        vi tmp;
        tmp.pb(v[0]);
        v.erase(v.begin());
        for(int i = 0; i < v.size(); i++){
            int u = v[i];
            if(u > tmp[tmp.size() - 1]){
                tmp.pb(u);
                v.erase(v.begin() + i);
                i--;
            }
        }
        r += tmp.size() - 1;
    }
    cout << r << endl;

    return 0;
}
