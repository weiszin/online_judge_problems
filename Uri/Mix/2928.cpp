#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int n; cin >> n;
    vector<string> vs;
    for(int i = 0; i < n; i++){
        string s; cin >> s;
        vs.push_back(s);
    }
    for(int i = 0; i < n - 2; i++){
        if(vs[i][0] == '.' and vs[i+1][0] == '.' and vs[i + 2][0] == '.'){
            cout << "N" << endl;
            return 0;
        }
    }
    int res = 0, flag = 0;
    for(int i = 0; i < n; i++){
        if(vs[i][0] == '.') flag = 1;
        else if(vs[i][0] == '-'){
            if(flag) res++;
            flag = 0;
        }
    }
    if(flag) res++;
    cout << res << endl;
    
    return 0;
}