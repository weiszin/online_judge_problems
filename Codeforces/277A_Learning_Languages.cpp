#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

vvi tmp, adj;
int vis[123];

void DFS(int u){
    vis[u] = 1;
    for(int v : adj[u]){
        if(!vis[v]) DFS(v);
    }
}

int main(int argc, char const *argv[]){
    int n, m;
    scanf("%d %d", &n, &m);
    adj.assign(n + 5, vi());
    tmp.assign(n + 5, vi());
    for(int i = 0; i < n; i++){
        int k;
        scanf("%d", &k);
        for(int j = 0; j < k; j++){
            int x;
            scanf("%d", &x);
            tmp[i].pb(x);
        }
        sort(tmp[i].begin(), tmp[i].end());
    }
    for(int i = 0; i < n; i++){
        for(int j = i + 1; j < n; j++){
            for(int u : tmp[i]){
                if(binary_search(tmp[j].begin(), tmp[j].end(), u)){
                    adj[i].pb(j);
                    adj[j].pb(i);
                    break;
                }
            }
        }
    }

    int cnt = 0;
    int nz = 0;
    for(int i = 0; i < n; i++){
        if(!vis[i]){
            if(tmp[i].size() > 0){
                DFS(i);
            } else {
                nz++;
            }
            cnt++;
        }
    }
    int res;
    if(nz == n) res = n;
    else res = cnt - 1;
    printf("%d\n", res);
    return 0;
}
