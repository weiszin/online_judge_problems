#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

vvi adj;
int vis[100005];
map<i2, int> mapa;
map<i2, int> tipo;
vector<i2> vet;

// [|] = 0 | [-] = 1 | [/] = 2 | [\] = 3

i2 nextPos(int i, int j, int tipo, int lado){
    i2 res;
    if(tipo == 0){
        res.first = i;
        if(lado == 0){
            res.second = j + 1;
        } else {
            res.second = j - 1;
        }
    } else if(tipo == 1){
        res.second = j;
        if(lado == 0){
            res.first = i + 1;
        } else {
            res.first = i - 1;
        }
    } else if(tipo == 2){
        if(lado == 0){
            res.first = i + 1;
            res.second = j + 1;
        } else {
            res.first = i - 1;
            res.second = j - 1;
        }
    } else {
        if(lado == 0){
            res.first = i + 1;
            res.second = j - 1;
        } else {
            res.first = i - 1;
            res.second = j + 1;
        }
    }

    return res;
}

int conflito(int a, int b){
    return !((a == 0 and b == 1) or (a == 1 and b == 0) or (a == 2 and b == 3) or (a == 3 and b == 2));
}

int solve(int u, int pai){
    vis[u] = 1;
    int res = 1;
    switch(tipo[vet[u]]){
        case 0:
            if(i2(vet[u].first - 1, vet[u].second - 1) == vet[pai] or
               i2(vet[u].first, vet[u].second - 1) == vet[pai] or
               i2(vet[u].first + 1, vet[u].second - 1) == vet[pai]){
                   if(adj[u][0] != -1 and !vis[adj[u][0]]){
                       res += solve(adj[u][0], u);
                   }
            }
            else if (i2(vet[u].first - 1, vet[u].second + 1) == vet[pai] or
                     i2(vet[u].first, vet[u].second + 1) == vet[pai] or
                     i2(vet[u].first + 1, vet[u].second + 1) == vet[pai]){
                if (adj[u][1] != -1 and !vis[adj[u][1]])
                    res += solve(adj[u][1], u);
            }
            break;
        case 1:
            if (i2(vet[u].first - 1, vet[u].second - 1) == vet[pai] or
                i2(vet[u].first - 1, vet[u].second) == vet[pai] or
                i2(vet[u].first - 1, vet[u].second + 1) == vet[pai]){
                if (adj[u][0] != -1 and !vis[adj[u][0]]) {
                    res += solve(adj[u][0], u);
                }
            }
            else if (i2(vet[u].first + 1, vet[u].second - 1) == vet[pai] or
                     i2(vet[u].first + 1, vet[u].second) == vet[pai] or
                     i2(vet[u].first + 1, vet[u].second + 1) == vet[pai]){
                if (adj[u][1] != -1 and !vis[adj[u][1]])
                    res += solve(adj[u][1], u);
            }
            break;
        case 2:
            if(i2(vet[u].first - 1, vet[u].second - 1) == vet[pai] or
               i2(vet[u].first - 1, vet[u].second) == vet[pai] or
               i2(vet[u].first, vet[u].second - 1) == vet[pai]){
                if (adj[u][0] != -1 and !vis[adj[u][0]])
                    res += solve(adj[u][0], u);
            }
            else if (i2(vet[u].first + 1, vet[u].second + 1) == vet[pai] or
                     i2(vet[u].first + 1, vet[u].second) == vet[pai] or
                     i2(vet[u].first, vet[u].second + 1) == vet[pai]){
                if (adj[u][1] != -1 and !vis[adj[u][1]])
                    res += solve(adj[u][1], u);
            }
            break;
        case 3:
            if (i2(vet[u].first - 1, vet[u].second + 1) == vet[pai] or
                i2(vet[u].first - 1, vet[u].second) == vet[pai] or
                i2(vet[u].first, vet[u].second + 1) == vet[pai]){
                if (adj[u][0] != -1 and !vis[adj[u][0]])
                    res += solve(adj[u][0], u);
            }
            else if (i2(vet[u].first + 1, vet[u].second - 1) == vet[pai] or
                     i2(vet[u].first + 1, vet[u].second) == vet[pai] or
                     i2(vet[u].first, vet[u].second - 1) == vet[pai])
                if (adj[u][1] != -1 and !vis[adj[u][1]])
                    res += solve(adj[u][1], u);
            break;
    }

    return res;
}

int main(){
    int n, idx, fx, fy;
    while(scanf("%d %d %d %d", &n, &idx, &fx, &fy)){
        if(n + idx + fx + fy == 0) break;
        swap(fx, fy);
        adj.assign(n + 5, vi());
        mapa.clear();
        tipo.clear();
        vet.clear();
        memset(vis, 0, sizeof vis);
        for(int i = 0; i < n; i++){
            int x, y;
            char c;
            scanf("%d %d %c", &x, &y, &c);
            swap(x, y);
            tipo[{x, y}] = (c == '|' ? 0 : (c == '-' ? 1 : (c == '/' ? 2 : 3)));
            // cout << "{" << x << ", " << y << "} = " << i << endl;
            mapa[{x, y}] = i;
            vet.push_back({x, y});
        }
        
        for(int i = 0; i < n; i++){
            i2 pos1 = nextPos(vet[i].first, vet[i].second, tipo[vet[i]], 0);
            i2 pos2 = nextPos(vet[i].first, vet[i].second, tipo[vet[i]], 1);
            // printf("vet: %d %d | pos1: %d %d | pos2: %d %d\n", vet[i].first, vet[i].second, pos1.first, pos1.second, pos2.first, pos2.second);
            if(mapa.count(pos1) > 0 and conflito(tipo[vet[i]], tipo[pos1])){
                adj[i].push_back(mapa[pos1]);
                // cout << "1: " << i << " -> " << mapa[pos1] << endl;
            } else {
                adj[i].push_back(-1);
            }
            if (mapa.count(pos2) > 0 and conflito(tipo[vet[i]], tipo[pos2])){
                adj[i].push_back(mapa[pos2]);
                // cout << "2: " << i << " -> " << mapa[pos2] << endl;
            }
            else {
                adj[i].push_back(-1);
            }
        }

        vis[idx] = 1;
        int res = 1;
        i2 proximo = { vet[idx].first + fx, vet[idx].second + fy };
        if (mapa.count(proximo) > 0 and conflito(tipo[vet[idx]], tipo[proximo])) res += solve(mapa[proximo], idx);
        cout << res << endl;
    }

    return 0;
}