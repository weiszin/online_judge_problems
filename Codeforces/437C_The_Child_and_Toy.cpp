#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min (a,b) ((a) < (b) ? (a) : (b))
#define max (a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;
typedef vector<i2> vi2;
typedef vector<vi2> vvi2;

int cost[1005];
int costAcc[1005];
int vis[1005];

int main(){
    int n, m; scanf("%d %d", &n, &m);
    for(int i = 0; i < n; i++) scanf("%d", &cost[i]);
    vvi adj(n + 5);
    for(int i = 0; i < m; i++){
        int x, y;
        scanf("%d %d", &x, &y);
        x--; y--;
        adj[x].push_back(y);
        adj[y].push_back(x);
        costAcc[x] += cost[y];
        costAcc[y] += cost[x];
    }

    ll res = 0;
    
    for(int i = 0; i < n; i++){
        int mini = 1e9;
        int idx = 0;
        for(int j = 0; j < n; j++){
            if(!vis[j])
                if(costAcc[j] < mini) mini = costAcc[j], idx = j;
                else if(costAcc[j] == mini and cost[j] > cost[idx]) idx = j;
        }
        res += mini;
        vis[idx] = 1;
        cout << "Tirei " << idx << endl;
        for(int u : adj[idx]) costAcc[u] -= cost[idx];
    }

    printf("%lld\n", res);

    return 0;
}

// 10 | 1 -> 20
// 20 | 2 -> 0
// 30 | 3 -> 20
// 40 | 4 -> 10

// 4, 1, 3, 2
