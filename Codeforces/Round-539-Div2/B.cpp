#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

vector<ll> vet; 

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    ll n; cin >> n;
    ll tot = 0;
    for(ll i = 0; i < n; i++){
        ll x; cin >> x;
        vet.push_back(x);
        tot += x;
    }
    
    sort(vet.rbegin(), vet.rend());
    ll mini = vet[vet.size() - 1];
    ll res = tot;
    for(ll i = 0; i < vet.size() - 1; i++){
        ll u = vet[i];
        for(ll j = 1; j <= sqrt(u); j++){
            if(u % j == 0){
                ll aux = j;
                res = min(res, tot - u + (u / aux) - mini + mini * aux);
                ll aux2 = u / j;
                if(aux2 != aux){
                    res = min(res, tot - u + (u / aux2) - mini + mini * aux2);
                }
            }
        }
    }
    cout << res << endl;

    return 0;
}