#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

map<int, int> comp;
vector<int> primos;
bitset<10000005> isPrime;
int vis[10000005];
int pref[10000005];

void crivo(){
    isPrime.set();
    primos.push_back(2);
    pref[2] += vis[2];
    // comp[2] = 1;
    for(int i = 4; i < 10000005; i += 2){
        isPrime[i] = 0;
        pref[2] += vis[i];
    }

    for(int i = 3; i < 10000005; i += 2){
        if(isPrime[i]){
            primos.push_back(i);
            pref[i] += vis[i];
            // comp[i] = primos.size();
            for(int j = i + i; j < 10000005; j += i){
                isPrime[j] = 0;
                pref[i] += vis[j];
            }
        }
    }
}


int main(){
    // ios_base::sync_with_stdio(false);
    // cin.tie(0);

    int n; cin >> n;
    for(int i = 0; i < n; i++){
        int x; scanf("%d", &x);
        // for(int u : primos){
        //     if(u > x) break;
        //     if(x % u == 0){
        //         pref[u]++;
        //         while(x % u == 0) x /= u;
        //     }
        // }
        // if(x > 1){
        //     pref[x]++;
        // }
        vis[x]++;
    }
    crivo();
    // cout << primos.size() << endl;
    for(int i = 1; i < 10000005; i++) pref[i] += pref[i - 1];
    // for(int i = 0; i < 10; i++) cout << pref[i] << endl;
    int k; cin >> k;
    for(int i = 0; i < k; i++){
        int l, r; scanf("%d %d", &l, &r);
        // int pos_r, pos_l;
        // pos_l = lower_bound(primos.begin(), primos.end(), l) - primos.begin() + 1;
        // pos_r = upper_bound(primos.begin(), primos.end(), r) - primos.begin();

        // cout << pos_l << " " << pos_r << " " << (pos_l-1 < primos.size() ? primos[pos_l-1] : -1) << " " << primos[pos_r-1] << endl;

        // cout << primos[pos_l - 1] << " " << primos[pos_r - 1] << " " << pos_l << " " << pos_r << endl;
        // if(pos_l > pos_r){
        //     printf("0\n");
        // } else {
        if(l > 1e7){
            printf("0\n");
        } else {
            r = min(1e7, r);
            printf("%d\n", pref[r] - pref[l - 1]);
        }
        // }

    }

    return 0;
}