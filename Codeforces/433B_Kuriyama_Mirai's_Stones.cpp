#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);

    int n;
    cin >> n;
    vector<ll> v;
    ll s1[n + 1], s2[n + 1];
    memset(s1, 0, sizeof s1);
    memset(s2, 0, sizeof s2);
    for(int i = 1; i <= n; i++){
        ll x;
        cin >> x;
        v.pb(x);
        s1[i] = s1[i - 1] + x;
    }
    sort(v.begin(), v.end());
    for(int i = 1; i <= n; i++){
        s2[i] = s2[i - 1] + v[i - 1];
    }
    int m;
    cin >> m;
    while(m--){
        int a, b, c;
        cin >> a >> b >> c;
        if(a == 1) cout << s1[c] - s1[b - 1] << endl;
        else cout << s2[c] - s2[b - 1] << endl;
    }

    return 0;
}
