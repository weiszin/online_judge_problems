#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef long long ll;

typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

int main(){
	ios_base::sync_with_stdio(false);

	int n;
	cin >> n;
	char m[n + 1][n + 1];
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			cin >> m[i][j];
		}
	}

	ll r = 0;
	for(int i = 0; i < n; i++){
		int c = 0;
		for(int j = 0; j < n; j++){
			c += (m[i][j] == 'C');
		}
		r += (c > 0 ? ((c  * (c - 1) / 2)) : 0);
	}
	for(int i = 0; i < n; i++){
		int c = 0;
		for(int j = 0; j < n; j++){
			c += (m[j][i] == 'C');
		}
		r += (c > 0 ? ((c  * (c - 1) / 2)) : 0);
	}
	cout << r << endl;
	return 0;
}
