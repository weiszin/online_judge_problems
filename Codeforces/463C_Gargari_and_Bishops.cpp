#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

ll mat[2005][2005];
ll lft[2005][2005], rght[2005][2005];

int main(){
    // ios_base::sync_with_stdio(false);
    // cin.tie(0);

    int n; cin >> n;
    for(int i = 1; i <= n; i++)
        for(int j = 1; j <= n; j++)
            scanf("%lld", &mat[i][j]);

    for(int i = 1; i <= n; i++){
        for(int j = 1; j <= n; j++){
            if(i == j){
                lft[0][0] += mat[i][j];
            } else if(i < j){
                lft[0][j - i] += mat[i][j];
            } else {
                lft[i - j][0] += mat[i][j];
            }
        }
    }
    // cout << "lft:" << endl;
    // for(int i = 0; i <= n + 1; i++){
    //     for(int j = 0; j <= n + 1; j++){
    //         cout << lft[i][j] << " ";
    //     } cout << endl;
    // } cout << endl;
    for(int i = 1; i <= n; i++){
        for(int j = 1; j <= n; j++){
            if(i + j - 1 == n){
                rght[0][n + 1] += mat[i][j];
            } else if(i + j - 1 > n){
                rght[i - (n - j + 1)][n + 1] += mat[i][j];
            } else {
                rght[0][j + i] += mat[i][j];
            }
        }
    }
    ll maxi_lft = -1, maxi_rght = -1;
    int xl, yl, xr, yr;
    for(int i = 1; i <= n; i++){
        for(int j = 1; j <= n; j++){
            ll tot = 0;
            if(i == j){
                tot += lft[0][0];
            } else if(i < j){
                tot += lft[0][j - i];
            } else {
                tot += lft[i - j][0];
            }
            if(i + j - 1 == n){
                tot += rght[0][n + 1];
            } else if(i + j - 1 > n){
                tot += rght[i - (n - j + 1)][n + 1];
            } else {
                tot += rght[0][j + i];
            }
            tot -= mat[i][j];
            if((i + j) % 2 == 0){
                if(tot > maxi_lft){
                    maxi_lft = tot;
                    xl = i;
                    yl = j;
                }
            } else {
                if(tot > maxi_rght){
                    maxi_rght = tot;
                    xr = i;
                    yr = j;
                }
            }
        }
    }
    cout << (maxi_lft + maxi_rght) << endl;
    cout << xl << " " << yl << " " << xr << " " << yr << endl;
    return 0;
}