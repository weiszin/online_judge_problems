#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAX 100000
#define mp make_pair
#define pb push_back
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define prtl(x) cout << x << endl;
#define prt(x) cout << x

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

int main(int argc, char const *argv[]){

    int n, m;
    cin >> n >> m;
    string a, b;
    cin >> a >> b;
    int cnt = INF;
    vi posF;
    for(int i = 0; i <= b.size() - a.size(); i++){
        int j;
        int dif = 0;
        vi pos;
        for(j = 0; i+j < b.size() and j < a.size(); j++){
            if(a[j] != b[i+j]){
                dif++;
                pos.pb(j);
            }
        }
        if(j == a.size()){
            if(dif < cnt){
                cnt = dif;
                posF = pos;
            }
        }
    }
    cout << cnt << endl;
    for(int v : posF){
        cout << v + 1 << " ";
    } cout << endl;

    return 0;
}
