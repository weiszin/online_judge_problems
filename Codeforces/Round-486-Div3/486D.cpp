#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef long long ll;

typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

map<ll, vector<ll> > adj;

int main(){
	ios_base::sync_with_stdio(false);
	int n;
	cin >> n;
	vi v;
	for(int i = 0; i < n; i++){
		int x;
		cin >> x;
		v.push_back(x);
	}
	sort(v.begin(), v.end());
	vector<ll> res;
	for(int i = 0; i < n; i++){
		ll x = v[i];
		vector<ll> tmp;
		adj[x].push_back(x);
		for(int j = 0; j <= 40; j++){
			ll vv = x + (1LL << j);
			if(binary_search(v.begin(), v.end(), vv)){
				adj[v].push_back(vv);
			}
		}
	}
	vector<ll> res;
    for(ii &p : adj)
        if(p.second.size() > res.size())
            res = par.second;
    cout << res.size() << endl;
    for(ll val : res)
        cout << val << " ";
	return 0;
}
