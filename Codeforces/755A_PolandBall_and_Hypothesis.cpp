#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define SWP(a,b) ((a) ^= (b) ^= (a) ^= (b))
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int isPrime(int v){
    for(int i = 2; i <= (int)sqrt(v); i++){
        if(v % i == 0) return 0;
    }
    return 1;
}

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);

    int n;
    cin >> n;
    for(int i = 1; i <= 10000; i++){
        if(!isPrime(n * i + 1)){
            cout << i << endl;
            break;
        }
    }

    return 0;
}
