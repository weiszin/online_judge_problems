#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define SWP(a,b) ((a) ^= (b) ^= (a) ^= (b))
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int adj[150][150];
int n;

int solve(int a, int b){
    int l = 0, r = 0;
    for(int i = a; i < b; i++){
        l += adj[i][i + 1];
    }
    int k = a;
    while(k != b){
        int nx = k - 1;
        if(nx < 0) nx = n - 1;
        r += adj[k][nx];
        k = nx;
    }
    return min(l, r);
}

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);
    cin >> n;
    vi v;
    for(int i = 0; i < n; i++){
        int x; cin >> x;
        adj[i][(i + 1) % n] = x;
        adj[(i + 1) % n][i] = x;
    }
    int a, b;
    cin >> a >> b;
    a--; b--;
    if(a > b) SWP(a, b);
    cout << solve(a, b) << endl;


    return 0;
}
