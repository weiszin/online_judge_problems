#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef tuple<int,int,int> i3;
typedef pair<ll,ll> pll;

int n;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    cin >> n;
    map<int, ii> mapa;
    int vis[1000005];
    int res = 0;
    memset(vis, 0, sizeof vis);
    for(int i = 0; i < n; i++){
        int x; cin >> x;
        if(!i){
            mapa[x] = ii(1, 0);
            vis[x] = 1;
        } else {
            if(vis[x]) continue;
            vis[x] = 1;
            if(mapa.count(x - 1) > 0){
                mapa[x] = ii(mapa[x-1].first+1, mapa[x-1].second+1);
            } else if(mapa.count(x - 2) > 0){
                mapa[x] = ii(1, mapa[x-2].first+1);
            } else {
                mapa[x] = ii(1, 0);
            }
        }
        res = max(res, max(mapa[x].first, mapa[x].second));
    }
    // for(int i = 0; i < l; i++){
    //     cout << vet[i].first << " " << vet[i].second << endl;
    // }
    cout << res + 1 << endl;
    return 0;
}

// (qtd_sequencia+1, qtd_sequencia+2)

/*
6
1       3       5       7       9       10      12      13      14      17     16
(1,0)  (1,2)   (1,2)   (1,2)   (1,2)   (2,2)   (1,3)   (2,2)  (3,3)   (1,0)   (1,4)

(1,0)  (1,1)   ()

*/