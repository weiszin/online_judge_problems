#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3f
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int n, m, x, y;
int mx[105][105];
int dp[105][105][21][21][5];

ll solve(int i, int j, int xx, int yy, int flag){
    if(xx > x or yy > y) return 1e9;
    if(i == (n - 1) and j == (m - 1)) {
        if(xx + (mx[i][j] == 0) > x or yy + (mx[i][j] < 0) > y) return 1e9;
        return mx[i][j];
    }
    if(dp[i][j][xx][yy][flag + 1] != INF) return dp[i][j][xx][yy][flag + 1];
    ll res = 1e9;
    if(i + 1 < n) res = min(res, solve(i + 1, j, (mx[i][j] == 0) + xx, (mx[i][j] < 0) + yy, 0) + mx[i][j]);
    if(j + 1 < m and flag != -1) res = min(res, solve(i, j + 1, (mx[i][j] == 0) + xx, (mx[i][j] < 0) + yy, 1) + mx[i][j]);
    if(j - 1 >= 0 and flag != 1) res = min(res, solve(i, j - 1, (mx[i][j] == 0) + xx, (mx[i][j] < 0) + yy, -1) + mx[i][j]);
    return dp[i][j][xx][yy][flag + 1] = res;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    while(cin >> n >> m >> x >> y){
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                cin >> mx[i][j];
            }
        }
        memset(dp, INF, sizeof dp);
        ll res = solve(0, 0, 0, 0, 0);
        if(res >= 1e7){
            cout << "Impossivel" << endl;
        } else {
            cout << res << endl;
        }
    }

    return 0;
}