#include <bits/stdc++.h>

using namespace std;

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int n;
    cin >> n;
    vector<int> vet;
    for(int i = 0; i < n; i++){
        int x;
        cin >> x;
        vet.push_back(x);
    }
    sort(vet.begin(), vet.end());
    int cnt = 0;
    for(int i = 1; i < n; i++){
        cnt += (vet[i] - vet[i - 1] - 1);
    }
    cout << cnt << endl;

    return 0;
}
