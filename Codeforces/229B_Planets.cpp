#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
vector<ii> vii;
typedef pair<ll,ll> pll;

vector<vector<pair<ll, ll> > > adj;
vector<vector<ll> > times;
ll n, m;

ll getTime(ll u, ll t){
    if(binary_search(times[u].begin(), times[u].end(), t)){
        ll pos = lower_bound(times[u].begin(), times[u].end(), t) - times[u].begin();
        ll res = 1;
        pos++;
        while(pos < times[u].size()){
            if(times[u][pos] == times[u][pos - 1] + 1){
                res++;
            } else break;
            pos++;
        }
        return res;
    } else return 0;
}

ll fft(){
    priority_queue<pair<ll,ll>, vector<pair<ll, ll> >, greater<pair<ll, ll> > > pq;
    ll tempo[100005];
    for(int i = 0; i <= n; i++){
        tempo[i] = 1e17;
    }
    tempo[1] = 0;
    pq.push({0, 1});
    while(!pq.empty()){
        pair<ll,ll> tmp = pq.top(); pq.pop();
        ll u = tmp.second;
        ll t = tmp.first;
        if(u == n) return t;
        ll cx = getTime(u, t);
        for(pair<ll,ll> p : adj[u]){
            ll v = p.first;
            ll c = p.second + cx;
            if(tempo[v] > t + c){
                tempo[v] = t + c;
                pq.push({t + c, v});
            }
        }
    }

    return -1;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cin >> n >> m;

    adj.assign(n + 5, vector<pair<ll,ll> >());
    times.assign(n + 5, vector<ll>());
    for(ll i = 0; i < m; i++){
        ll x, y, z;
        cin >> x >> y >> z;
        adj[x].push_back({y,z});
        adj[y].push_back({x,z});
    }
    
    for(ll i = 1; i <= n; i++){
        ll k; cin >> k;
        for(ll j = 0; j < k; j++){
            ll x; cin >> x;
            times[i].push_back(x);
        }
    }
    
    cout << fft() << endl;

    return 0;
}