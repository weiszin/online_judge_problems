#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAX 100000
#define mp make_pair
#define pb push_back
#define FOR(i, s, n) for(int i = s; i < n; i++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define prtl(x) cout << x << endl;
#define prt(x) cout << x

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

int main(int argc, char const *argv[]){

    int n;
    cin >> n;
    set<int> s;
    FOR(i, 0, n){
        int x; cin >> x;
        s.insert(x);
    }
    deque<int> dq;
    for(auto &i : s){
        // cout << i << " ";
        if(dq.empty()) dq.pb(i);
        else if(i - dq.back() == 1) dq.pb(i);
        else if(i - dq.back() != 1) dq.clear(), dq.pb(i);
        // for(auto &u : dq) cout << u << " ";
        // cout << endl;
        if(dq.size() == 3){
            cout << "YES" << endl;
            return 0;
        }
    }
    cout << "NO" << endl;
    return 0;
}
