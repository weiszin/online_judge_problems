#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef long long ll;

typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

vvi adj;

void DFS(int u, int *vis){
	vis[u] = 1;
	for(int v : adj[u]){
		if(!vis[v]) DFS(v, vis);
	}
}

int main(){
	ios_base::sync_with_stdio(false);
	vii v;
	int n;
	cin >> n;
	for(int i = 0; i < n; i++){
		int x, y;
		cin >> x >> y;
		v.push_back({x, y});
	}

	adj.assign(n + 5, vi());
	for(int i = 0; i < n; i++){
		for(int j = i + 1; j < n; j++){
			if(v[i].first == v[j].first or v[i].second == v[j].second){
				adj[i].push_back(j);
				adj[j].push_back(i);
			}
		}
	}

	int vis[n + 5];
	memset(vis, 0, sizeof vis);
	int r = 0;
	for(int i = 0; i < n; i++){
		if(!vis[i]) DFS(i, vis), r++;
	}
	cout << r - 1 << endl;
	return 0;
}
