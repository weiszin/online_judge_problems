#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define MOD 1000000007
#define PI acos(-1)
#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))
#define pb push_back

typedef long long ll;
typedef unsigned long long ull;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<ii> vii;
typedef vector<vii> vvii;

int pref1[1100], pref[1100];

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);

    int n, m, q;
    cin >> n >> m >> q;
    string s, t;
    cin >> s >> t;
    for(int i = 0; i < n; i++){
        if(s.substr(i, m) == t) pref1[i + 1] = 1;
    }
    for(int i = 1; i <= n; i++){
        pref[i] = pref[i - 1] + pref1[i];
    }
    for(int i = 0; i < q; i++){
        int a, b;
        cin >> a >> b;
        if(b - a + 1 >= m){
            cout << pref[b - m + 1] - pref[a - 1] << endl;
        } else cout << 0 << endl;
    }

    return 0;
}
