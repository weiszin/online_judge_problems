#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    for(int k = 11; k <= 20; k++){
        cout << "k = " << k << endl;
        for(int i = 0; i < k; i++){
            cout << i << ":" << (k^i) << " ";
        } cout << endl;
    }

    return 0;
}