#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

map<pair<queue<int>, queue<int> >, int> mapa;

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
	
	int n;
	cin >> n;
	queue<int> a, b;
	int k;
	cin >> k;
	for(int i = 0; i < k; i++){
		int x;
		cin >> x;
		a.push(x);
	}
	cin >> k;
	for(int i = 0; i < k; i++){
		int x;
		cin >> x;
		b.push(x);
	}
		
	mapa[{a, b}] = 1;
	int cnt = 0;
	while(!a.empty() and !b.empty()){
		int at = a.front(); a.pop();
		int bt = b.front(); b.pop();
		if(at > bt){
			a.push(bt);
			a.push(at);
		} else {
			b.push(at);
			b.push(bt);
		}
		if(mapa.find({a,b}) != mapa.end()) break;
		mapa[{a,b}] = 1;
		cnt++;
	}
	
	int aa = a.empty();
	int bb = b.empty();
	if(aa or bb){
		cout << cnt << " " << (aa ? "2" : "1") << endl;
	} else {
		cout << -1 << endl;
	}

    return 0;
}
