#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int v[10005];
int dp[10005][10005][2];

ii MAX(ii &a, ii &b){
    if(a.first == b.first){
        if(a.second < b.second) return a;
        return b;
    } else {
        if(a.first > b.first) return a;
        return b;
    }
}

ii MAX_REV(ii &a, ii &b){
    if(a.first == b.first){
        if(a.second > b.second) return a;
        return b;
    } else {
        if(a.first < b.first) return a;
        return b;
    }
}

ii SOMA(ii &a, ii &b){
    return {a.first + b.first, a.second + b.second};
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    int n;
    while(cin >> n){
        for(int i = 0; i < n; i++)
            cin >> v[i];
        int flag = 0;
        for(int i = 0; i < n; i++) dp[i][i][flag] = v[i], dp[i][i][!flag] = -v[i];
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                // for(int k = 0; k < 2; k++){
                dp[i][j][0] = dp[i][j - 1][1] + v[j];
                dp[i][j][1] = dp[i][j - 1][0] - v[j];
                // }
            }
        }
        for(int i = n-1; i >= 0; i--)
            for(int j = i-1; j >= 0; j--){
                dp[i][j][0] = max(dp[i][j][0], dp[i + 1][j][1] + v[i]);
                dp[i][j][1] = min(dp[i][j][1], dp[i + 1][j][0] - v[i]);
            }
        cout << dp[0][0][0] << endl;
    }
    
    return 0;
}