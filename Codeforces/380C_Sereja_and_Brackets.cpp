#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

struct P {
    int tam, abre, fecha;
    P(){}
    P(int _tam, int _abre, int _fecha){
        tam = _tam;
        abre = _abre;
        fecha = _fecha;
    }

    P operator + (const P &other){
        int t = min(abre, other.fecha);
        return P(tam + other.tam + t, abre + other.abre - t, fecha + other.fecha - t);
    }
};

string s;
P tree[4000005];

void build(int l, int r, int pos){
    if(l == r){
        tree[pos] = P(0, s[l] == '(', s[l] == ')');
        // cout << "Pos: " << pos << " " << tree[pos].tam << " " << tree[pos].abre << " " << tree[pos].fecha << endl;
        return;
    }
    int mid = (l + r) / 2;
    build(l, mid, 2 * pos + 1);
    build(mid + 1, r, 2 * pos + 2);
    tree[pos] = tree[2 * pos + 1] + tree[2 * pos + 2];
    // cout << "Pos: " << pos << " " << tree[pos].tam << " " << tree[pos].abre << " " << tree[pos].fecha << endl;
}

P query(int l, int r, int L, int R, int pos){
    if(l > R or r < L) return P(0,0,0);
    if(l >= L and r <= R) return tree[pos];
    int mid = (l + r) / 2;
    return (query(l, mid, L, R, 2 * pos + 1) + query(mid + 1, r, L, R, 2 * pos + 2));
}

int main(){
    cin >> s;
    build(0, s.size() - 1, 0);
    int q; cin >> q;
    while(q--){
        int a, b; cin >> a >> b;
        a--; b--;
        cout << query(0, s.size() - 1, a, b, 0).tam*2 << endl;
    }
    
    return 0;
}