#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

string s;
int n, k;
int dp[55][35][55];

int solve(int pos, int last, int cnt){
    if(pos == s.size()){
        return (cnt == k ? 0 : 1e9);
    }
    int &aux = dp[pos][last][cnt];
    if(aux != -1) return aux;
    int res = solve(pos + 1, last, cnt);
    if(last == 0){
        res = min(res, solve(pos + 1, (s[pos] - 'a') + 1, cnt + 1) + (s[pos] - 'a' + 1));
    } else {
        if((s[pos] - 'a' + 1) - last >= 2){
            res = min(res, solve(pos + 1, (s[pos] - 'a') + 1, cnt + 1) + (s[pos] - 'a' + 1));
        }
    }
    return aux = res;
}

int main(int argc, char const *argv[]){
    scanf("%d %d", &n, &k);
    cin >> s;
    s.erase(std::unique(s.begin(), s.end()), s.end());
    sort(s.begin(), s.end());
    memset(dp, -1, sizeof dp);
    int res = solve(0, 0, 0);
    if(res < 1e9) printf("%d\n", res);
    else printf("-1\n");

    return 0;
}
