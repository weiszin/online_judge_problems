#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int vet[405];
int cnt[1005];
int mat[25][25];

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int n; cin >> n;
    for(int i = 0; i < n * n; i++){
        cin >> vet[i];
        cnt[vet[i]]++;
    }
    sort(vet, vet + (n * n));
    int flag = 1;
    if(n % 2 == 0){
        for(int k = 0; k < n * n; k++){
            if(cnt[vet[k]] % 4 != 0){
                flag = 0;
            }
        }
        if(flag){
            int pos = 0;
            for(int i = 0; i < n; i++){
                for(int j = 0; j < n; j++){
                    if(mat[i][j] == 0){
                        mat[i][j] = vet[pos++];
                        mat[i][n - j - 1] = vet[pos++];
                        mat[n - i - 1][j] = vet[pos++];
                        mat[n - i - 1][n - j - 1] = vet[pos++];
                    }
                }
            }
        }
    } else {
        for(int i = 0; i < n / 2; i++){
            for(int j = 0; j < n / 2; j++){
                int ok = 0;
                for(int k = 0; k < n * n; k++){
                    if(cnt[vet[k]] > 0 and cnt[vet[k]] >= 4){
                        int num = vet[k];
                        mat[i][j] = num;
                        mat[i][n - j - 1] = num;
                        mat[n - i - 1][j] = num;
                        mat[n - i - 1][n - j - 1] = num;
                        cnt[num] -= 4;
                        ok = 1;
                        break;
                    }
                }
                flag &= ok;
            }
        }
        int cm = n / 2;
        for(int i = 0; i < n / 2; i++){
            int ok = 0;
            for(int k = 0; k < n * n; k++){
                if(cnt[vet[k]] > 0 and cnt[vet[k]] >= 2){
                    int num = vet[k];
                    mat[i][cm] = num;
                    mat[n - i - 1][cm] = num;
                    cnt[num] -= 2;
                    ok = 1;
                    break;
                }
            }
            flag &= ok;
        }
        int lm = n / 2;
        for(int j = 0; j < n / 2; j++){
            int ok = 0;
            for(int k = 0; k < n * n; k++){
                if(cnt[vet[k]] > 0 and cnt[vet[k]] >= 2){
                    int num = vet[k];
                    mat[lm][j] = num;
                    mat[lm][n - j - 1] = num;
                    cnt[num] -= 2;
                    ok = 1;
                    break;
                }
            }
            flag &= ok;
        }
        for(int k = 0; k < n * n; k++){
            if(cnt[vet[k]] != 0) mat[lm][cm] = vet[k];
        }
    }
    if(flag){
        cout << "YES" << endl;
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                cout << mat[i][j] << " ";
            } cout << endl;
        }
    } else {
        cout << "NO" << endl;
    }
    return 0;
}