#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int vv[35];
vii vet;
int d, n;
int dp[35][250];

int solve(int pos, int soma){
    if(pos == d) return (soma == n);
    if(soma > n) return 0;
    if(dp[pos][soma] != -1)return dp[pos][soma];
    for(int i = vet[pos].second; i >= vet[pos].first; i--){
        vv[pos] = i;
        if(solve(pos + 1, soma + i)){
            return 1;
        }
    }
    return dp[pos][soma] = 0;
}

int main(int argc, char const *argv[]){
    scanf("%d %d", &d, &n);
    int sumL = 0, sumR = 0;
    for(int i = 0; i < d; i++){
        int l, r;
        scanf("%d %d", &l, &r);
        vet.push_back({l, r});
        sumL += l;
        sumR += r;
    }
    if(n > sumR or n < sumL) printf("NO\n");
    else {
        printf("YES\n");
        memset(dp, -1, sizeof dp);
        solve(0, 0);
        for(int i = 0; i < d; i++){
            printf("%d ", vv[i]);
        } printf("\n");
    }
    return 0;
}
