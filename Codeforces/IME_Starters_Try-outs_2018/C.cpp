#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2*acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'
#define eb emplace_back
#define pb push_back

typedef pair<int,int> ii;
typedef pair<int,ii> iii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<iii> viii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

vector<vector<iii> > adj;
int n, r;

int custoReset(vector<vector<char> > &v){
	int tot = r;
	for(int i = 0; i < 21; i++){
		for(int j = 0; j < 21; j++){
			if(v[i][j] == 1) tot++;
		}
	}
	return tot;
}

int custoCont(vector<vector<char> > &a, vector<vector<char> > &b){
	int tot = 0;
	for(int i = 0; i < 21; i++){
		for(int j = 0; j < 21; j++){
			if(a[i][j] != b[i][j]) tot++;
		}
	}
	return tot;
}

int start(vector<vector<char> > &v){
	int tot = 0;
	for(int i = 0; i < 21; i++){
		for(int j = 0; j < 21; j++){
			if(v[i][j] == 1) tot++;
		}
	}
	return tot;
}

int bestRes = INF;
vi bestResVet;
int dp[20][1 << 20];

void TSP(int daVez, int custo, vector<int> &v, int bitmask){
	if(bitmask == ((1 << n) - 1)){
		if(custo < bestRes){
			bestRes = custo;
			bestResVet = v;
		}
		return;
	}
	if(dp[daVez][bitmask] != -1) return;
	
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);	
	cin >> n >> r;
	ll tot = 0;
	vector<vector<vector<char> > > vet(n + 1);
	for(int i = 0; i < n; i++){
		vector<vector<char> > v(25);
		for(int j = 0; j < 21; j++){
			for(int k = 0; k < 21; k++){
				char c;
				cin >> c;
				v[j].eb(c);
			}
		}
		vet.eb(v);
	}

	adj.assign(n + 5, vector<iii>());
	for(int i = 0; i < n; i++){
		for(int j = i + 1; j < n; j++){
			int reset = custoReset(vet[j]);
			int cont = custoCont(vet[i], vet[j]);
			if(reset < cont){
				adj[i].pb({j, {reset, 1}});
				adj[j].pb({i, {reset, 1}});
			} else {
				adj[i].pb({j, {cont, 0}});
				adj[j].pb({i, {cont, 0}});
			}
		}
	}
	memset(dp, -1, sizeof dp);
	vi vetor;
	for(int i = 0; i < n; i++){
		TSP(i, start(vet[i]), vetor, (1 << i));
	}
	cout << bestRes << endl;
	for(int u : bestResVet){
		if(u < 0) cout << "* ";
		else cout << u << " ";
	} cout << endl;

	return 0;
}
