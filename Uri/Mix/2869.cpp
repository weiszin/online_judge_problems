#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

vector<ll> primos;
void crivo(){
    bitset<1005> isPrime;
    isPrime.set();
    for(ll i = 2; i < 1005; i++){
        if(isPrime[i]){
            primos.push_back(i);
            for(ll j = i + i; j < 1005; j += i) isPrime[j] = 0;
        }
    }
}

ll mod = 1e9+7;
ll x;
pair<ll,ll> res;

ll expo(ll b, ll e){
    ll res = 1;
    while(e > 0){
        if(e & 1){
            res = (res * b) % mod;
            e--;
        }
        b = (b * b) % mod;
        e /= 2;
    }
    return res;
}

void solve_aux(vector<int> &vet){
    ll qtd_modulo = 0;
    ll number = 1;
    // cout << "Resolvendo para vet = ";
    // for(int u : vet) cout << u << " ";
    // cout << endl;
    for(int i = 0; i < vet.size(); i++){
        int u = vet[i];
        // cout << "Primo[" << i  << "] = " << primos[i] << endl;
        for(int j = 0; j < u - 1; j++){
            number *= primos[i];
            qtd_modulo += (number / mod);
            // cout << number << " " << mod << " " << qtd_modulo << endl;
            number %= mod;
        }
    }
    // cout << "other_res = {" << qtd_modulo << ", " << number << "}" << endl;
    pair<ll,ll> other_res(qtd_modulo, number);
    res = min(res, other_res);
}

void solve(int pos, int acc, vector<int> &vet){
    if(acc > x or pos > x) return;
    if(acc == x){
        solve_aux(vet);
        return;
    }
    if(acc * pos == x){
        vet.push_back(pos);
        solve_aux(vet);
        vet.pop_back();
        return;
    }
    solve(pos + 1, acc, vet);
    vet.push_back(pos);
    solve(2, acc * pos, vet);
    vet.pop_back();
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int n; cin >> n;
    crivo();
    while(n--){
        cin >> x;
        if(x == 1) cout << 1 << endl;
        else {
            vector<int> vet;
            res = pair<ll,ll>(1e9, 1e9);
            solve(2, 1, vet);
            cout << res.second << endl;
        }
    }
    
    return 0;
}