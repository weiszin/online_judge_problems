#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef long long ll;

typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

int main(){
	ios_base::sync_with_stdio(false);
	vii neg, pos;
	int n;
	cin >> n;
	for(int i = 0; i < n; i++){
		int x, y;
		cin >> x >> y;
		if(x < 0){
			neg.push_back({x, y});
		} else {
			pos.push_back({x, y});
		}
	}

	sort(neg.rbegin(), neg.rend());
	sort(pos.begin(), pos.end());

	int maxi = min(neg.size(), pos.size());

	int r = 0;
	for(int i = 0; i < maxi; i++){
		r += neg[i].second;
		r += pos[i].second;
	}
	if(pos.size() > maxi){
		r += pos[maxi].second;
	} else if(neg.size() > maxi){
		r += neg[maxi].second;
	}

	cout << r << endl;

	return 0;
}
