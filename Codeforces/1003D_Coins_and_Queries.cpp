#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a, b) ((a) < (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

int cnt[105];

int solve(int x){
    int y = floor(log2(x));
    int res = 0;
    while(x > 0 and y >= 0){
        if(cnt[y]){
            int aux = cnt[y];
            if((1 << y) <= x){
                int tmp = min(aux, floor(x / (1 << y)));
                res += tmp;
                x -= (tmp * (1 << y));
            }
        }
        y--;
    }
    if(x > 0) return -1;
    return res;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, q; scanf("%d %d", &n, &q);
    for(int i = 0; i < n; i++){
        int x; scanf("%d", &x);
        cnt[(int)log2(x)]++;
    }
    while(q--){
        int x;
        scanf("%d", &x);
        printf("%d\n", solve(x));
    }
    
    return 0;
}