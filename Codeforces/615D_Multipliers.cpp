#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<ll,ll> ii;
typedef pair<ll,ll> pll;

ll mod = 1e9+7;
ll phi = mod-1;

ll pref[200005], suff[200005];

ll modpow(ll b, ll e){
    if(e == 0) return 1;
    if(e == 1) return (b%mod);
    ll res = modpow(b, e/2);
    res = (res*res)%mod;
    if(e%2 == 1) res = (res*(b%mod))%mod;
    return res;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    map<ll,ll> mapa;
    
    ll n; cin >> n;
    for(ll i = 0; i < n; i++){
        ll x; cin >> x;
        mapa[x]++;
    }

    vector<ii> vec(mapa.begin(), mapa.end());
    ll k = vec.size();
    pref[0] = suff[k+1] = 1;

    for(ll i = 1; i <= k; i++)
        pref[i] = (pref[i-1] * (vec[i-1].second+1))%phi;
    // for(ll i = 1; i <= k; i++)
    //     cout << pref[i] << " ";
    // cout << endl;
    for(ll i = k; i >= 1; i--)
        suff[i] = (suff[i+1] * (vec[i-1].second+1))%phi;
    // for(ll i = 1; i <= k; i++)
    //     cout << suff[i] << " ";
    // cout << endl;
    
    ll res = 1;
    for(ll i = 1; i <= k; i++){
        ll a = pref[i-1];
        ll b = suff[i+1];
        ll c = (a*b)%phi;
        ll tot = 0;
        for(ll j = 1; j <= vec[i-1].second; j++)
            tot = (tot + c*j)%phi;
        //cout << vec[i-1].first << " aparece " << tot << " vezes" << endl;
        res = (res*modpow(vec[i-1].first, tot))%mod;
    }
    cout << res << endl;

    return 0;
}