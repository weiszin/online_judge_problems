#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;
typedef pair<ll,ll> pll;

vector<pll> precos;
int contagemVotos[3005];

int main(){
    int n, m; cin >> n >> m;
    for(int i = 0; i < n; i++){
        ll p, c; cin >> p >> c;
        precos.push_back({c, p});
        contagemVotos[p]++;
    }
    sort(precos.begin(), precos.end());
    ll res = 1e17;
    for(int i = 1; i <= n; i++){
        int contagemVotosAux[3005];
        int vis[3005];
        memset(vis, 0, sizeof vis);
        for(int j = 0; j < 3005; j++) contagemVotosAux[j] = contagemVotos[j];
        ll resAux = 0;
        int cnt = i - (contagemVotos[1]);
        for(int j = 0; j < n and cnt > 0; j++){
            int v = precos[j].second;
            if(v == 1) continue;
            ll p = precos[j].first;
            if(contagemVotosAux[v] >= i){
                contagemVotosAux[v]--;
                resAux += p;
                cnt--;
                vis[j] = 1;
            }
        }
        if(cnt > 0){
            for(int j = 0; j < n and cnt > 0; j++){
                if(precos[j].second == 1) continue;
                if(!vis[j] and contagemVotosAux[precos[j].second] > 0){
                    cnt--;
                    contagemVotosAux[precos[j].second]--;;
                    resAux += precos[j].first;
                }
            }
        }
        int ok = 1;
        for(int j = 2; j <= m; j++)
            if(contagemVotosAux[j] >= i) ok = 0;
        if(ok) res = min(res, resAux);
    }

    cout << res << endl;
    
    return 0;
}