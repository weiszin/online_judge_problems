#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2*acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'
#define eb emplace_back
#define pb push_back

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

int ord(const ii &a, const ii &b){
    return a.second < b.second;
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);

    int n, m;
    cin >> n >> m;
    vvi adj(n + 1);
    for(int i = 0; i < m; i++){
        int x, y;
        cin >> x >> y;
        x--; y--;
        adj[x].pb(y);
        adj[y].pb(x);
    }
    vector<ii> vetor;
    for(int i = 0; i < n; i++){
        vetor.push_back({i, adj[i].size()});
    }

    sort(vetor.begin(), vetor.end(), ord);

    int vis[n + 5];
    memset(vis, 0, sizeof vis);
    int r = 0;
    for(ii &p : vetor){
        if(!vis[p.first]){
            r++;
            for(int u : adj[p.first]){
                vis[u] = 1;
            }
        }
    }
    cout << max(r, n - r) << endl;
	return 0;
}
