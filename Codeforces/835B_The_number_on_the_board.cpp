#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'
#define eb emplace_back

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
	
	int k;
	cin >> k;
	string s;
	cin >> s;
	ll sum = 0;
	for(char c : s){
		sum += (c - '0');
	}
	if(sum >= k) cout << 0 << endl;
	else {
		sort(s.begin(), s.end());
		int r = 0;
		int p = 0;
		while(sum < k){
			sum += (9 - (s[p++] - '0'));
			r++;
		}
		cout << r << endl;
	}
	return 0;
}
