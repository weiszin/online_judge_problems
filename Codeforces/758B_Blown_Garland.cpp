#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef long long ll;

typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

int main(){
	ios_base::sync_with_stdio(false);

	string s;
	cin >> s;
	int v[4];
	memset(v, 0, sizeof v);

	char k[4] = {'R', 'B', 'Y', 'G'};

	for(int i = 0; i < 4; i++){
		for(int j = 0; j < s.size(); j++){
			if(s[j] == k[i]){
				for(int l = j + 4; l < s.size(); l += 4){
					v[i] += (s[l] == '!');
				}
				for(int l = j - 4; l >= 0; l -= 4){
					v[i] += (s[l] == '!');
				}
				break;
			}
		}
	}

	cout << v[0] << " " << v[1] << " " << v[2] << " " << v[3] << endl;

	return 0;
}
