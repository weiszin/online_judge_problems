#include <bits/stdc++.h>

using namespace std;

int marca[200005];

int main(int argc, char const *argv[]) {
    cout.sync_with_stdio(false); cin.tie(0);

    int n, m, d;
    cin >> n >> m >> d;
    set<pair<int, int> > s;
    for(int i = 0; i < n; i++){
        int x;
        cin >> x;
        s.insert({x, i});
    }
    int t = 1;
    int day = 1;
    while(!s.empty()){
        t = (*s.begin()).first;
        marca[(*s.begin()).second] = day;
        s.erase(s.begin());
        while(!s.empty()){
            auto it = lower_bound(s.begin(), s.end(), make_pair(t + d + 1, 0));
            if(it == s.end()) break;
            marca[it -> second] = day;
            t = it -> first;
            s.erase(it);
        }
        day++;
    }
    cout << day - 1 << endl;
    for(int i = 0; i < n; i++){
        cout << marca[i] << " ";
    }
    cout << endl;
    return 0;
}
