#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef long long ll;

typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

int main(){
	ios_base::sync_with_stdio(false);

	ll n, k, s;
	cin >> n >> k >> s;
	ll aux = ceil(s / (double)k);
	if(aux >= n or k > s) cout << "NO" << endl;
	else {
		ll a = 1;
		ll ss = 0;
		cout << "YES" << endl;
		int p = 0;
		while(ss != s){
			if(!p) {
				ss += aux;
				a += aux;
				p = 1;
				cout << a << " ";
			} else {
				ss += aux;
				a -= aux;
				p = 0;
				cout << a << " ";
			}
			k--;
			ll falta = s - ss;
			aux = ceil(falta / (double)k);
		}
		cout << endl;
	}


	return 0;
}
