#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

/*
        99999
    999999999
    ---------
    000099998
*/

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    string a, b; cin >> a >> b;
    string res = "";
    
    int i = a.size() - 1, j = b.size() - 1;
    int aux = 0, next;
    while(i >= 0 and j >= 0){
        int f = a[i] - '0';
        int s = b[j] - '0';
        next = (f + s + aux) % 10;
        aux = (f + s + aux) / 10;
        res += to_string(next);
        i--; j--;
    }
    while(i >= 0){
        int f = a[i] - '0';
        next = (f + aux) % 10;
        aux = (f + aux) / 10;
        res += to_string(next);
        i--;
    }
    while(j >= 0){
        int s = b[j] - '0';
        next = (s + aux) % 10;
        aux = (s + aux) / 10;
        res += to_string(next);
        j--;
    }
    if(aux > 0){
        string aa = to_string(aux);
        // reverse(aa.begin(), aa.end());
        res += aa;
    }
    reverse(res.begin(), res.end());
    cout << res << endl;
    return 0;
}