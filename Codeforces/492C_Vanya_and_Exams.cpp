#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAX 100000
#define mp make_pair
#define pb push_back
#define FOR(i, s, n) for(int i = s; i < n; i++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define prtl(x) cout << x << endl;
#define prt(x) cout << x

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

// (u + x) / n >= avg
// u + x >= n * avg
// x >= n * avg - u

int ord(pair<ll, ll> &a, pair<ll, ll> &b){return a.second < b.second;}

int main(int argc, char const *argv[]){
    ll n, r, avg;
    cin >> n >> r >> avg;
    vector<pair<ll, ll> > vet;
    ll tot = 0;
    FOR(i, 0, n){
        ll a, b;
        cin >> a >> b;
        tot += a;
        vet.pb({a, b});
    }
    double m = tot / (double)n;
    if(m >= avg){
        cout << 0 << endl;
        return 0;
    }
    sort(vet.begin(), vet.end(), ord);
    ll res = 0;
    for(auto &u : vet){
        ll qtd = r - u.first;
        ll custo = u.second;
        ll x = min(qtd, n * avg - tot);
        // cout << x << endl;
        res += (x * custo);
        tot += x;
        m = tot / (double)n;
        if(m >= avg) break;
    }
    cout << res << endl;
    return 0;
}
