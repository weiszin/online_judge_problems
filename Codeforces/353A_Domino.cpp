#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);
    int n;
    cin >> n;
    vii vet;
    int l = 0, r = 0;
    for(int i = 0; i < n; i++){
        int x, y;
        cin >> x >> y;
        l += x; r += y;
        vet.pb({x, y});
    }
    if(l % 2 == 0 and r % 2 == 0) cout << 0 << endl;
    else {
        int f = 0;
        for(ii u : vet){
            if((l - u.first + u.second) % 2 == 0 and (r - u.second + u.first) % 2 == 0){
                f = 1;
                break;
            }
        }
        if(!f) cout << -1 << endl;
        else cout << 1 << endl;
    }

    return 0;
}
