#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2*acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'
#define eb emplace_back

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

double tree[4 * 100100];
double vet[100100];

void build(int p, int l, int r){
	if(l == r){
		tree[p] = vet[l];
	} else {
		int mid = (l + r) / 2;
		build(2 * p + 1, l, mid);
		build(2 * p + 2, mid + 1, r);
		tree[p] = tree[2 * p + 1] + tree[2 * p + 2];
	}
}

void update(int p, int l, int r, int obj, double n){
	if(l == r and l == obj){
		tree[p] = n;
	} else {
		if(l > obj or r < obj) return;
		int mid = (l + r) / 2;
		update(2 * p + 1, l, mid, obj, n);
		update(2 * p + 2, mid + 1, r, obj, n);
		tree[p] = tree[2 * p + 1] + tree[2 * p + 2];
	}
}

double query(int p, int l, int r, int ql, int qr){
	if(l > qr or r < ql) return 0.0;
	else {
		if(l >= ql and r <= qr){
			return tree[p];
		} else {
			int mid = (l + r) / 2;
			return query(2 * p + 1, l, mid, ql, qr) + query(2 * p + 2, mid + 1, r, ql, qr);
		}
	}
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
	
	int n;
	cin >> n;
	for(int i = 0; i < n; i++){
		cin >> vet[i];
	}

	build(0, 0, n - 1);
	int k;
	cin >> k;
	while(k--){
		int o;
		cin >> o;
		if(o == 1){
			int a;
			double b;
			cin >> a >> b;
			a--;
			update(0, 0, n - 1, a, b);
		} else {
			int a, b;
			cin >> a >> b;
			a--; b--;
			double r = query(0, 0, n - 1, a, b);
			printf("%.6f\n", r / (double)(b - a + 1.0));
		}
	}

	return 0;
}
