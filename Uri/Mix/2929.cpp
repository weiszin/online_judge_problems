#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int n; cin >> n;
    stack<int> pilha;
    multiset<int> ms;
    while(n--){
        string s; cin >> s;
        if(s == "PUSH"){
            int x; cin >> x;
            pilha.push(x);
            ms.insert(x);
        } else if(s == "POP"){
            if(pilha.empty()) cout << "EMPTY" << endl;
            else {
                int x = pilha.top();
                pilha.pop();
                auto it = ms.find(x);
                ms.erase(it);
            }
        } else {
            if(pilha.empty()) cout << "EMPTY" << endl;
            else {
                cout << *(ms.begin()) << endl;
            }
        }
    }
    
    return 0;
}