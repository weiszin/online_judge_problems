#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef long long ll;

typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

map<int, int> mapaX;
map<int, int> mapaY;

int superCentral(ii &p){

}

int main(){
	ios_base::sync_with_stdio(false);

	int n;
	cin >> n;
	vii v;
	for(int i = 0; i < n; i++){
		int x, y;
		cin >> x >> y;
		v.push_back({x, y});
	}

	int res = 0;

	for(int i = 0; i < n; i++){
		int l = 0, r = 0, b = 0, up = 0;
		ii u = v[i];
		for(int j = 0; j < n; j++){
			if(i == j) continue;
			ii p = v[j];
			if(u.second == p.second){
				if(u.first > p.first) r = 1;
				if(u.first < p.first) l = 1;
			}
			if(u.first == p.first){
				if(u.second > p.second) up = 1;
				if(u.second < p.second) b = 1;
			}
		}
		if(l and r and b and up) res++;
	}

	cout << res << endl;

	return 0;
}
