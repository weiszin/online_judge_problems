#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int n, m;
int dp[1005][1005];
vector<int> vet;

int solve(int pos, int k){
    if(pos >= n) return (k == 0);
    if(k == 0) return 1;
    int& d = dp[pos][k];
    if(d != -1) return d;
    int res = solve(pos + 1, k);
    res |= solve(pos + 1, (k + vet[pos]) % m);
    return d = res;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    cin >> n >> m;
    for(int i = 0; i < n; i++){
        int x; cin >> x;
        vet.push_back(x);
    }
    if(n >= m){
        cout << "YES" << endl;
        return 0;
    }
    memset(dp, -1, sizeof dp);
    int res = 0;
    for(int i = 0; i < n; i++){
        res |= solve(i + 1, vet[i] % m);
    }
    if(res) cout << "YES" << endl;
    else cout << "NO" << endl;
    return 0;
}