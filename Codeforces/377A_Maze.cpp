#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

char mapa[510][510];
int vis[510][510];
int dx[] = {0, 1, -1, 0};
int dy[] = {1, 0, 0, -1};
int n, m, k;

int valid(int a, int b){
    return (a >= 0 and b >= 0 and a < n and b < m);
}

void solve(int x, int y){
    vis[x][y] = 1;
    for(int i = 0; i < 4; i++){
        int xx = x + dx[i];
        int yy = y + dy[i];
        if(valid(xx, yy) and !vis[xx][yy] and mapa[xx][yy] == '.'){
            solve(xx, yy);
        }
    }
    if(k > 0){
        k--;
        mapa[x][y] = 'X';
    }
}

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);
    cin >> n >> m >> k;

    for(int i = 0; i < n; i++){
        for(int j = 0; j < m; j++){
            char c;
            cin >> mapa[i][j];
        }
    }
    int f = 1;
    for(int i = 0; i < n and f; i++){
        for(int j = 0; j < m and f; j++){
            if(mapa[i][j] == '.'){
                solve(i, j);
                f = 0;
            }
        }
    }

    for(int i = 0; i < n; i++){
        for(int j = 0; j < m; j++){
            cout << mapa[i][j];
        } cout << endl;
    }

    return 0;
}
