#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int vet[200005];
int prefL[200005], prefR[200005], suffL[200005], suffR[200005];

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int n; cin >> n;
    for(int i = 1; i <= n; i++) cin >> vet[i];
    // sort(vet + 1, vet + n + 1);
    for(int i = 1; i <= n; i++){
        if(i % 2 == 0){
            prefL[i] = prefL[i - 1];
            prefR[i] = prefR[i - 1] + vet[i];
        } else {
            prefR[i] = prefR[i - 1];
            prefL[i] = prefL[i - 1] + vet[i];
        }
    }
    for(int i = n; i >= 1; i--){
        if(i % 2 == 0){
            suffL[i] = suffL[i + 1];
            suffR[i] = suffR[i + 1] + vet[i];
        } else {
            suffR[i] = suffR[i + 1];
            suffL[i] = suffL[i + 1] + vet[i];
        }
    }
    int res = 0;
    for(int i = 1; i <= n; i++){
        int f = prefL[i - 1] + suffR[i + 1];
        int s = prefR[i - 1] + suffL[i + 1];
        // cout << i << " " << f << " " << s << endl;
        if(f == s) res++;
    }
    cout << res << endl;

    return 0;
}