#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

ll fact[1000005], invfact[1000005];
ll mod = 1e9+7;

ll expo(ll b, ll e){
    ll res = 1;
    while(e > 0){
        if(e & 1) res = (res * b) % mod;
        b = (b * b) % mod;
        e /= 2;
    }
    return res;
}

ll comb(ll n, ll k){
    if(k > n) return 0;
    ll up = fact[n];
    ll down = (invfact[n-k]*invfact[k])%mod;
    return (up*down)%mod;
}

ll arranjo(ll n, ll k){
    if(k > n) return 0;
    ll up = fact[n];
    ll down = invfact[n-k];
    return (up*down)%mod;
}

// 17 223 10 8
// 210685217

int main(){


    ll n, m, x, y; scanf("%lld %lld %lld %lld", &n, &m, &x, &y);

    fact[0] = 1; invfact[0] = 1;
    for(ll i = 1; i <= 1000000; i++){
        fact[i] = (fact[i-1]*i)%mod;
        invfact[i] = expo(fact[i], mod-2);
    }
    
    ll res = fact[n-2]*comb(m-1, n-2);
    
    for(ll k = 0; k <= n-3; k++){
        ll a = arranjo(n-2, k); // todos os possiveis elementos no meio
        ll b = (a*comb(m-1, k))%mod; // todos os possiveis pesos das arestas
        ll c = (b*expo(m, n-2-k))%mod; // todas as possiveis arestas restantes
        ll d = (c*expo(n, n-1-(k+2)))%mod; // todas as arvores possiveis
        ll e = (d*(k+2))%mod;
        res += e; res %= mod;
    }
    cout << res << endl;
    return 0;
}
