#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef long long ll;

typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

int n, m;
vector<string> v;
int vis[110][110];

int conta(int a, int b, int aa, int bb){
	int c = 0;
	a += aa;
	b += bb;
	while(a >= 0 and b >= 0 and a < n and b < m and v[a][b] == '*'){
		c++;
		a += aa;
		b += bb;
	}
	return c;
}

int solve(int a, int b){
	int l = conta(a, b, -1, 0);
	int r = conta(a, b, 1, 0);
	int u = conta(a, b, 0, -1);
	int bt = conta(a, b, 0, 1);
	int res = min(l, r);
	res = min(res, u);
	res = min(res, bt);
	return res;
}

int verify(){
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			if(v[i][j] == '*' and !vis[i][j]) return 0;
		}
	}
	return 1;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin >> n >> m;
	for(int i = 0; i < n; i++){
		string s;
		cin >> s;
		v.push_back(s);
	}
	vector<pair<int, pair<int, int> > > vet;
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			if(v[i][j] == '*'){
				int aux = solve(i, j);
				if(aux >= 1){
					vet.push_back({i + 1, {j + 1, aux}});
					for(int k = -aux; k <= aux; k++){
						vis[i + k][j] = 1;
						vis[i][j + k] = 1;
					}
				}
			}
		}
	}
	int aaux = verify();
	if(aaux){
		cout << vet.size() << endl;
		for(pair<int, pair<int, int> > a : vet){
			cout << a.first << " " << a.second.first << " " << a.second.second << endl;
		}
	} else {
		cout << -1 << endl;
	}
	return 0;
}
