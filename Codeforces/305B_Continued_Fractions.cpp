#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<ll, ll> ii;

ll a[105], n;
vector<ll> vet;

void solve(ll upper, ll bottom, ll pos){
    // if(pos == n + 1) return 1;
    if (upper == 0 or bottom == 0)
        return;
    ll tmp = floor(upper / bottom);
    vet.push_back(tmp);
    ll rest = upper % bottom;
    // cout << tmp << " " << rest << endl;
    // if(tmp != a[pos]) return 0;
    solve(bottom, rest, pos + 1);
}

int main(){

    ll p, q;
    cin >> p >> q;
    cin >> n;
    for (int i = 1; i <= n; i++)
        cin >> a[i];
    if (n > 1 and a[n] == 1){
        a[n - 1]++;
        n--;
    }
    solve(p, q, 1);
    int ans = (vet.size() == n);
    for (int i = 0; i < n; i++)
        ans &= (vet[i] == a[i + 1]);
    if (ans)
        cout << "YES" << endl;
    else
        cout << "NO" << endl;

    return 0;
}