#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int values[2000005];

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    set<int> s;
    int n; cin >> n;
    int res = 0;
    for(int i = 0; i < n; i++){
        int x;
        cin >> x;
        if(values[x] == 0) res++;
        values[x] = 1;
        vector<int> v;
        v.push_back(x);
        for(int y : s){
            int r = y | x;
            if(values[r] == 0) res++;
            values[r] = 1;
            v.push_back(r);
        }
        s.clear();
        for(int u : v) s.insert(u);
    }
    cout << res << endl;

    return 0;
}