#include <bits/stdc++.h>
using namespace std;
typedef pair<int, int> ii;

int idx_b[200005], a[200005], b[200005], idx_a[200005];

int main()
{
    int n;
    cin >> n;

    for (int i = 1; i <= n; i++)
        cin >> a[i];
    for (int i = 1; i <= n; i++)
    {
        cin >> b[i];
        idx_b[b[i]] = i;
    }

    for (int i = 1; i <= n; i++)
        a[i] = idx_b[a[i]];
    int pos = 1;
    for (int i = 1; i <= n; i++, pos++)
        if (a[i] > a[i + 1])
            break;
    cout << n - pos << endl;
    return 0;
}
