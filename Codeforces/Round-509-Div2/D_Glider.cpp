#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<ll,ll> ii;

vector<ii> vec;
ll pref[200005];

int main(){
    ll n, h; cin >> n >> h;
    for(ll i = 0; i < n; i++){
        ll x, y; cin >> x >> y;
        vec.push_back({x,y});
    }
    pref[0] = 0;
    for(ll i = 1; i < n; i++)
        pref[i] = pref[i-1] + vec[i].first - vec[i-1].second;
    pref[n] = 1e18;

    ll res = 0;
    for(ll i = 0; i < n; i++){
        ll esq = vec[i].first;
        ll altura = h + pref[i];
        ll pos = upper_bound(pref, pref+n+1, altura)-pref-1;
        ll dist;
        if(altura-pref[pos] == 0)
            dist = vec[pos].first-esq;
        else
            dist = vec[pos].second+altura-pref[pos]-esq;
        res = max(res, dist);
    }
    cout << res << endl;


    return 0;
}
