#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
int a, b;

int findK(int xf, int yf, int x0, int y0){
    if(xf != 0){
        if((a - x0) % xf != 0){
            return 0;
        }
        int k = (a - x0) / xf;
		if(k < 0) return 0;
        if(yf * k + y0 == b) return 1;
    } else if(yf != 0){
        if((b - y0) % yf != 0){
            return 0;
        }
        int k = (b - y0) / yf;
		if(k < 0) return 0;
        if(xf * k + x0 == a) return 1;
    }
    return 0;
}

int main(){
    cout.sync_with_stdio(false); cin.tie(nullptr);
    cin >> a >> b;
    string s; cin >> s;
    int offX = 0, offY = 0;
    vector<ii> vec;
    vec.push_back({0,0});
    int f = 0;
    for(char c : s){
        if(offX == a and offY == b) f = 1;
        if(c == 'U') offY++;
        if(c == 'D') offY--;
        if(c == 'L') offX--;
        if(c == 'R') offX++;
        if(offX == a and offY == b) f = 1;
        vec.push_back({offX, offY});
    }
    if(f) cout << "Yes" << endl;
    else{
        for(ii par : vec){
            int val = findK(offX, offY, par.first, par.second);
            if(val) f = 1;
        }
        if(f) cout << "Yes" << endl;
        else cout << "No" << endl;
    }

    return 0;
}
