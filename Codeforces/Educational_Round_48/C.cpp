#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define MOD 1000000007
#define PI acos(-1)
#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))
#define pb push_back

typedef long long ll;
typedef unsigned long long ull;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef pair<ii, ii> iiii;
typedef vector<ii> vii;
typedef vector<vii> vvii;

int matriz[2][320000];

int di[] = {0, 1, -1, 0};
int dj[] = {1, 0, 0, -1};

int vis[2][320000];
int n;

int valid(int a, int b){
    if(a >= 0 and b >= 0 and a < 2 and b < n){
        int f = 0;
        if(b + 1 < n and vis[a][b + 1]) f = 1;
        else if(b - 1 >= 0 and vis[a][b - 1]) f = 1;
        else if(b + 1 >= n) f = 1;
        else if(b - 1 < 0) f = 1;
        return f;
    }
    return 0;
}

ll solve(int a, int b, int tempo, int c){
    if (c >= 3) return 0;
    vis[a][b] = 1;
    ll res = 0;
    for(int i = 0; i < 4; i++){
        int aa = a + di[i];
        int bb = b + dj[i];
        if(valid(aa, bb) and !vis[aa][bb]){
            res = max(res, solve(aa, bb, tempo + 1, c + 1) + matriz[aa][bb] * tempo);
        }
    }
    vis[a][b] = 0;
    return res;
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    cin >> n;
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < n; j++){
            cin >> matriz[i][j];
        }
    }

    int a = 0, b = 0;
    ll res = 0;
    for(int i = 1; i < 2 * n; i++){
        vector<pair<ll, ii> > vet;
        vis[a][b] = 1;
        for(int j = 0; j < 4; j++){
            int aa = a + di[j];
            int bb = b + dj[j];
            if(valid(aa, bb) and !vis[aa][bb]){
                ll calc = solve(aa, bb, i, 0);
                vet.push_back({calc, {aa, bb}});
            }
        }
        sort(vet.rbegin(), vet.rend());
        a = vet[0].second.first;
        b = vet[0].second.second;
        // cout << matriz[a][b] << endl;
        res += matriz[a][b] * i;
    }
    cout << res << endl;
    return 0;
}
