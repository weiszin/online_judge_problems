#include <iostream>
#include <map>
#include <cmath>

using namespace std;

int main(){
    string s; cin >> s;

    map<char,int> mapa;
    for(char c : s){
        mapa[c]++;
    }
    int res = 0;
    while(1){
        if(mapa['n'] >= 3 and mapa['i'] >= 1 and mapa['e'] >= 3 and mapa['t'] >= 1){
            mapa['n'] -= 2;
            mapa['i']--;
            mapa['e'] -= 3;
            mapa['t']--;
            res++;
        } else break;
    }
    // int maxi = min(floor(mapa['n']/3), min(floor(mapa['i']), min(floor(mapa['e']/3), floor(mapa['t']))));
    cout << res << endl;
    return 0;
}