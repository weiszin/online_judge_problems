#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef tuple<int,int,int> i3;
typedef pair<ll,ll> pll;

vector<vector<int> > adj;
map<ii, int> distancias;
vector<i3> vet;
int pai[100005];
int super_pai[100005];
int pai_lca[100005];
int super_distancia[100005];
int distancia[100005];
int vis[100005];
int nivel_lca[100005];

int find_pai(int u){
    if(pai[u] == u) return u;
    return pai[u] = find_pai(pai[u]);
}
void union_f(int u, int v){
    pai[u] = v;
}

int tam_arvore, sqrt_arvore;

void tam_arvore_func(int u, int n){
    vis[u] = 1;
    tam_arvore = max(tam_arvore, n);
    for(int v : adj[u]){
        if(!vis[v]){
            tam_arvore_func(v, n + 1);
        }
    }
}

void monta_lca(int u, int ppai, int spai, int maior_aresta, int nivel){
    vis[u] = 1;
    nivel_lca[u] = nivel;
    pai_lca[u] = ppai;
    super_pai[u] = spai;
    super_distancia[u] = maior_aresta;
    distancia[u] = distancias[{u,ppai}];
    if(nivel % sqrt_arvore == 0){
        spai = u;
        maior_aresta = 0;
    }
    for(int v : adj[u]){
        if(!vis[v]){
            int dist = distancias[{u,v}];
            int maxi = max(maior_aresta, dist);
            monta_lca(v, u, spai, maxi, nivel + 1);
        }
    }
}

int query(int u, int v){
    int res = 0;
    while(super_pai[u] != super_pai[v]){
        // cout << "Super pais diferentes: " << super_pai[u] << " " << super_pai[v] << endl;
        if(nivel_lca[u] > nivel_lca[v]){
            res = max(res, super_distancia[u]);
            // cout << "Indo de " << u << " para ";
            u = super_pai[u];
            // cout << u << endl;
        } else {
            res = max(res, super_distancia[v]);
            // cout << "Indo de " << v << " para ";
            v = super_pai[v];
            // cout << v << endl;
        }
    }
    while(u != v){
        // cout << u << " " << v << endl;
        if(nivel_lca[u] > nivel_lca[v]){
            res = max(res, distancia[u]);
            // cout << "Indo de " << u << " para ";
            u = pai_lca[u];
            // cout << u << endl;
        } else {
            res = max(res, distancia[v]);
            // cout << "Indo de " << v << " para ";
            v = pai_lca[v];
            // cout << v << endl;
        }
    }
    return res;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n, m; cin >> n >> m;

    adj.assign(n + 5, vector<int>());

    for(int i = 0; i < m; i++){
        int u, v, w; cin >> u >> v >> w;
        vet.push_back(i3(w, u, v));
    }
    sort(vet.begin(), vet.end());
    for(int i = 0; i <= n; i++) pai[i] = i;
    for(i3 p : vet){
        int w, u, v; tie(w, u, v) = p;
        int pu = find_pai(u), pv = find_pai(v);
        if(pu != pv){
            union_f(pu, pv);
            adj[u].push_back(v);
            adj[v].push_back(u);
            distancias[{u,v}] = distancias[{v,u}] = w;
        }
    }
    // cout << "oi1" << endl;
    tam_arvore_func(0, 0);
    // cout << "oi2" << endl;
    sqrt_arvore = sqrt(tam_arvore);
    memset(vis, 0, sizeof vis);
    monta_lca(0, 0, 0, 0, 1); //u, pai, super_pai, maior_aresta ateh super_pai

    int q; cin >> q;
    while(q--){
        int u, v; cin >> u >> v;
        // u = entrada[u];
        // v = entrada[v];
        // if(u > v) swap(u, v);
        // cout << query(u,v, 0, tour.size(), 0) << endl;
        cout << query(u, v) << endl;
    }

    return 0;
}