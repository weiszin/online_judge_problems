#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAX 100000
#define mp make_pair
#define pb push_back
#define FOR(i, s, n) for(int i = s; i < n; i++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define prtl(x) cout << x << endl;
#define prt(x) cout << x

typedef pair<int,int> i2;
typedef vector<int> vi;
typedef vector<i2> vi2;
typedef vector<vi> vvi;
typedef vector<vector<i2> > vvi2;
typedef long long ll;

vvi adj;
vi vetorzao;
int marcaC[234567], marcaT[234567];

void DFS(int u){
    vetorzao.pb(u);
    marcaC[u] = vetorzao.size() - 1;
    // cout << "Comeca " << u + 1 << ": " << marcaC[u] << endl;
    for(int v : adj[u]){
        DFS(v);
    }
    marcaT[u] = vetorzao.size() - 1;
    // cout << "Termina " << u + 1 << ": " << marcaT[u] << endl;
}

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false); cin.tie(NULL);

    int n, q;
    cin >> n >> q;
    adj.assign(n + 1, vi());
    FOR(i, 1, n){
        int x;
        cin >> x;
        x--;
        adj[x].pb(i);
    }
    for(int i = 0; i < n; i++){
        sort(adj[i].begin(), adj[i].end());
    }
    DFS(0);
    // for(int u : vetorzao) cout << u << " ";
    // cout << endl;
    FOR(i, 0, q){
        int a, b;
        cin >> a >> b;
        a--;
        int comeca = marcaC[a];
        int termina = marcaT[a];
        if(termina - comeca + 1 < b) cout << "-1" << endl;
        else cout << (vetorzao[comeca + b - 1] + 1) << endl;
    }
    return 0;
}
