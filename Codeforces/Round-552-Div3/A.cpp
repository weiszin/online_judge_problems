#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    ll vet[4];
    for(int i = 0; i < 4; i++){
        cin >> vet[i];
    }
    
    sort(vet, vet + 4);
    ll tmp[3];
    tmp[2] = vet[3] - vet[0];
    tmp[1] = vet[3] - vet[1];
    tmp[0] = vet[3] - vet[2];
    do {
        if(tmp[0] + tmp[1] == vet[0] and tmp[0] + tmp[2] == vet[1] and tmp[1] + tmp[2] == vet[2]){
            cout << tmp[0] << " " << tmp[1] << " " << tmp[2] << endl;
            return 0;
        }
    } while(next_permutation(tmp, tmp + 3));

    return 0;
}