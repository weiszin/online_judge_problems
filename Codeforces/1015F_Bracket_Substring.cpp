#include <bits/stdc++.h>
using namespace std;

int n, dp[205][205];

int precalc(int pos, int f, int maxi, int fmaxi){
    if(pos == maxi) return (f == fmaxi);
    int res = precalc(pos+1, f+1, maxi, fmaxi);
    if(f > 0)
        res += precalc(pos+1, f-1, maxi, fmaxi);
    return res;
}

int main(){
    cout.sync_with_stdio(false); cin.tie(nullptr);
    cin >> n; n *= 2;
    string s; cin >> s;
    int f = 0, len = s.size();
    for(char c : s){
        if(c == '(') f++;
        else f--;
    }

    for(int i = 1; i <= n; i++)
        for(int j = 0; j <= n; j++)
            dp[i][j] = precalc(0, 0, i, j);

    if(f < 0){
        f = abs(f);
        int res = 0;
        for(int i = 0; i <= n; i++)
            for(int j = f; j <= n; j++)
                if(i+len <= n){
                    printf("dp[%d][%d] = %d * dp[%d][%d] = %d\n",i, j, dp[i][j], n-i-len, j-f, dp[n-i-len][j-f]);
                    res += dp[i][j]*dp[n-i-len][j-f];
                }
        cout << res << endl;
    }

    return 0;
}
