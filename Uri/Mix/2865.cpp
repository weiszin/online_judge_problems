#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int palin[1000005];

int isPalin(int x){
    string bin = "";
    while(x > 0){
        bin += to_string(x % 2);
        x /= 2;
    }
    int l = 0, r = bin.size() - 1;
    while(l < r){
        if(bin[l] != bin[r]) return palin[x] = 0;
        l++; r--;
    }
    return palin[x] = 1;
}

struct Node {
    int palin, best;
    char op;

    Node(){
        this -> palin = -1;
    }

    Node(int palin, int best, char op){
        this -> palin = palin;
        this -> best = best;
        this -> op = op;
    }
};

Node dp[10005];

void solve(int x){
    int best = 1e9;
    char op;
    // Soma
    int aux = x;
    for(int i = 1; i <= 100000; i++){
        aux++;
        if(palin[aux] == -1){
            if(isPalin(aux)){
                if(i < best){
                    best = i;
                    op = '+';
                }
                break;
            }
        } else {
            if(palin[aux]){
                if(i < best){
                    best = i;
                    op = '+';
                    break;
                }
            }
        }
    }
    //Sub
    aux = x;
    int cnt = 0;
    while(aux >= 0){
        aux--;
        cnt++;
        if(cnt >= best) break;
        if(palin[aux] == -1){
            if(isPalin(aux)){
                best = cnt;
                op = '-';
                break;
            }
        } else {
            if(palin[aux]){
                best = cnt;
                op = '-';
                break;
            }
        }
    }

    //Multi
    for(int i = 2; i <= 100000; i++){
        aux = x * i;
        if(i >= best) break;
        if(palin[aux] == -1){
            if(isPalin(aux)){
                best = i;
                op = 'x';
                break;
            }
        } else {
            if(palin[aux]){
                best = i;
                op = 'x';
                break;
            }
        }
    }

    //Div
    cnt = 0;
    aux = x;
    for(int i = 2; i <= 10000; i++){
        if(i >= best) break;
        int res = (int)(aux / i);
        if(palin[res] == -1){
            if(isPalin(res)){
                best = i;
                op = '/';
                break;
            }
        } else {
            if(palin[res]){
                best = i;
                op = '/';
                break;
            }
        }
    }
    // dp[x].palin = 0;
    // dp[x].best = best;
    // dp[x].op = op;
    printf("%c %d\n", op, best);
}

void pre(){
    for(int i = 1; i <= 100000; i++){
        if(isPalin(i)) palin[i] = 1;
        else palin[i] = 0;
    }
}

int main(){
    // ios_base::sync_with_stdio(false);
    // cin.tie(0);

    int t; cin >> t;
    memset(palin, -1, sizeof palin);
    pre();
    while(t--){
        int x; cin >> x;
        if(palin[x] == 1){
            printf("*\n");
        } else if(palin[x] == -1){
            // if(dp[x].palin != -1){
            //     printf("%c %d\n", dp[x].op, dp[x].best);
            // } else {
            if(isPalin(x)){
                printf("*\n");
            } else {
                solve(x);
            }
        } else {
            // if(dp[x].palin != -1){
            //     printf("%c %d\n", dp[x].op, dp[x].best);
            // } else {
            solve(x);
        }
    }
    
    
    return 0;
}