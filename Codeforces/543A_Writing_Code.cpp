#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

int n, m, b, mod;
int peso[505];
int dp[2][505][505];

// posicao / quantidade linha / bugs

int solve(int pos, int l, int bugs){
    if(bugs > b) return 0;
    if(pos == n and l > 0) return 0;
    if(l == 0) return 1;
    int &d = dp[pos][l][bugs];
    if(d != -1) return d;
    int res1 = solve(pos + 1, l, bugs) % mod;
    int res2 = solve(pos, l - 1, bugs + peso[pos]) % mod;
    return d = (res1 + res2)%mod;
}

int main(){
    cin >> n >> m >> b >> mod;
    for(int i = 0; i < n; i++) cin >> peso[i];
    
    for(int i = 0; i <= m; i++){
        for(int j = b; j >= 0; j--){
            // if(j + i * peso[n - 1] > b) break;
            dp[1][i][j] = (j + i * peso[n - 1] <= b) % mod;
        }
    }
    for(int k = n - 2; k >= 0; k--){
        for(int i = 0; i <= b; i++) dp[0][0][i] = 1;
        for(int i = 1; i <= m; i++){
            for(int j = b; j >= 0; j--){
                dp[0][i][j] = dp[1][i][j] % mod;
                if(j + peso[k] <= b) dp[0][i][j] = (dp[0][i][j]%mod + dp[0][i - 1][j + peso[k]]%mod)%mod;
            }
        }
        for(int i = 0; i <= m; i++)
            for(int j = 0; j <= b; j++) dp[1][i][j] = dp[0][i][j];
    }

    // for(int i = 0; i < n; i++){
    //     for(int j = 0; j <= m; j++){
    //         for(int k = 0; k <= b; k++){
    //             cout << dp[i][j][k] << endl;
    //         }
    //     }
    // }
    cout << dp[1][m][0] << endl;

    return 0;
}