#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;
string a, b;
int dp[1005][1005];

int solve(int pa, int pb){
    if(pa >= a.size() or pb >= b.size()) return 0;
    if(dp[pa][pb] != -1) return dp[pa][pb];
    int res = max(solve(pa + 1, pb), solve(pa, pb + 1));
    if(a[pa] == b[pb]) res = max(res, solve(pa + 1, pb + 1) + 1);
    return dp[pa][pb] = res;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cin >> a >> b;
    if(a.size() > b.size()) swap(a, b);
    memset(dp, -1, sizeof dp);
    int res = solve(0, 0);
    if(res == a.size()){
        cout << b.size() << endl;
    } else {
        cout << b.size() + (a.size() - res) << endl;
    }
    return 0;
}