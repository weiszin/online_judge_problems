#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

ll mod = 1e9+7;

void multiply(ll a[3][3], ll b[3][3]){
    int matriz_aux[3][3];
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            ll aux = 0;
            for(int k = 0; k < 3; k++){
                aux = (aux + (a[i][k] * b[k][j]) % mod) % mod;
            }
            matriz_aux[i][j] = aux;
        }
    }
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            a[i][j] = matriz_aux[i][j];
        }
    }
}

ll solve(ll expo){
    ll base[3][3] = {{0, 1, 1}, {1, 0, 0}, {0, 1, 0}};
    ll tmp[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
    while(expo > 0){
        if(expo & 1){
            multiply(tmp, base);
            expo--;
        }
        expo /= 2;
        multiply(base, base);
    }
    ll res = (tmp[0][0] * 2 + tmp[0][1] * 2 + tmp[0][2]) % mod;
    return res;
}

int main(){

    ll n;
    while(cin >> n){
        if(n <= 3){
            if(n == 1) cout << 1 << endl;
            else if(n == 2 or n == 3) cout << 2 << endl; 
        } else {
            cout << solve(n - 3) << endl;
        }
    }


	return 0;
}
