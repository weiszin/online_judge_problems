#include <bits/stdc++.h>

using namespace std;

#define INF (((1LL) << (31)) - 1)
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'

typedef long long ll;

typedef pair<int,int> ii;
typedef pair<int, ii> iii;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

vvi adj;
int soma[200100];

class No {
public:
	int v, idxSeq, idxVal;
	No(int v, int idxSeq, int idxVal){
		this -> v = v;
		this -> idxSeq = idxSeq;
		this -> idxVal = idxVal;
	}
};

int ord(const No &a, const No &b){
	if(a.v == b.v){
		if(a.idxSeq == b.idxSeq){
			return a.idxVal < b.idxVal;
		}
		return a.idxSeq < b.idxSeq;
	}
	return a.v < b.v;
}

int main(){
	ios_base::sync_with_stdio(false);

	int k;
	cin >> k;

	adj.assign(k + 1, vi());

	for(int i = 0; i < k; i++){
		int n;
		cin >> n;
		for(int j = 0; j < n; j++){
			int x;
			cin >> x;
			soma[i] += x;
			adj[i].push_back(x);
		}
	}
	vector<No> v;
	for(int i = 0; i < k; i++){
		for(int j = 0; j < adj[i].size(); j++){
			v.push_back(No(soma[i] - adj[i][j], i, j));
		}
	}

	sort(v.begin(), v.end(), ord);
	int f = 0;
	for(int i = 0; i < v.size() - 1 and !f; i++){
		if(v[i].v == v[i + 1].v and v[i].idxSeq != v[i + 1].idxSeq){
			cout << "YES" << endl;
			cout << v[i].idxSeq + 1 << " " << v[i].idxVal + 1 << endl << v[i + 1].idxSeq + 1 << " " << v[i + 1].idxVal + 1 << endl;
			f = 1;
		}
	}
	if(!f) cout << "NO" << endl;

	return 0;
}
