from math import *

maxi = 2000005
primos = []

def crivo():
    global primos
    isPrime = [1] * maxi
    primos = [2]
    for i in range(4, maxi, 2):
        isPrime[i] = 0
    for i in range(3, maxi, 2):
        if(isPrime[i] == 1):
            primos.append(i)
            for j in range(i + i, maxi, i):
                isPrime[j] = 0

crivo()
n, b = [int(x) for x in input().split()]

res = (1 << 58)
j = b
for i in primos:
    if(i > b): break
    if(j % i == 0):
        p = 0
        while(j % i == 0):
            p += 1
            j = floor(j / i)
        c = 0
        k = n
        while(floor(k / i) > 0):
            c += floor(k / i)
            k = floor(k / i)
        tmp = floor(c / p)
        if(tmp < res): res = tmp
if(res == (1 << 58)):
    res = floor(n / b)
print(res)