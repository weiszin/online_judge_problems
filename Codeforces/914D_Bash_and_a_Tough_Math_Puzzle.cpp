#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int left(int u){return (u << 1);}
int right(int u){return (u << 1) + 1;}


int tree[2000005];

void update(int u, int L, int R, int pos, int x){
    if(L == R) tree[u] = x;
    else{
        int mid = (L+R)/2;
        if(pos <= mid) update(left(u), L, mid, pos, x);
        else update(right(u), mid+1, R, pos, x);
        tree[u] = __gcd(tree[left(u)], tree[right(u)]);
    }
}

int query(int u, int L, int R, int i, int j, int x){
    if(R < i || j < L) return x;
    if(L >= i and R <= j) return tree[u];
    int mid = (L+R)/2;
    int r1 = query(left(u), L, mid, i, j, x);
    int r2 = query(right(u), mid+1, R, i, j, x);
    return __gcd(r1, r2);
}

int fft(int u, int L, int R, int i, int j, int x){
    if(R < i || j < L) return 0;
    if(L >= i and R <= j){
        if(L == R) return (tree[u]%x == 0 ? 0 : 1);
        int mid = (L+R)/2;
        if(tree[left(u)]%x != 0 and tree[right(u)]%x != 0) return 2;
        else if(tree[left(u)]%x != 0 and tree[right(u)]%x == 0) return fft(left(u), L, mid, i, j, x);
        else if(tree[left(u)]%x == 0 and tree[right(u)]%x != 0) return fft(right(u), mid+1, R, i, j, x);
        else return 0;
    }
    else{
        int mid = (L+R)/2;
        int r1 = fft(left(u), L, mid, i, j, x);
        if(r1 > 1) return r1;
        int r2 = fft(right(u), mid+1, R, i, j, x);
        return r1+r2;
    }
}

int main(){
    // ios_base::sync_with_stdio(false);
    // cin.tie(0);

    int n; cin >> n;
    for(int i = 1; i <= n; i++){
        int x; //cin >> x;
        scanf("%d", &x);
        update(1, 1, n, i, x);
    }

    int q; cin >> q;
    while(q--){
        int op; cin >> op;
        if(op == 1){
            int esq, dir, x;// cin >> esq >> dir >> x;
            scanf("%d %d %d", &esq, &dir, &x);
            int g = query(1, 1, n, esq, dir, x);
            if(g%x == 0) printf("YES\n"); //cout << "YES" << endl;
            else{
                int cnt = fft(1, 1, n, esq, dir, x);
                if(cnt >= 2) printf("NO\n"); //cout << "NO" << endl;
                else printf("YES\n"); //cout << "YES" << endl;
            }
        }
        else{
            int pos, x; //cin >> pos >> x;
            scanf("%d %d", &pos, &x);
            update(1, 1, n, pos, x);
        }
    }
    
    
    return 0;
}