#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define INFL (1LL << 58)
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))
#define GET(x,t) get<x>(t)

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;
typedef vector<ii> vii;
typedef vector<vii> vvii;
typedef tuple<int,int,int> i3;
typedef tuple<ll,ll,ll> ll3;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);    
    
    return 0;
}