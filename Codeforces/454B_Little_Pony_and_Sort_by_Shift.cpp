#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'
#define eb emplace_back

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int n;
	cin >> n;
	vi v;
	int menor = INF;
	int pos = 0;
	for(int i = 0; i < n; i++){
		int x;
		cin >> x;
		if(x < menor) menor = x, pos = i;
		v.eb(x);
	}
	if(pos == 0){
		int aux = n - 1;
		while(aux != pos and v[aux] == v[pos]) aux--;
		if(aux != pos){
			pos = (aux + 1) % n;
		}
	}
	int aux = pos;
	aux = (aux + 1) % n;
	int f = 1;
	while(aux != pos and f){
		if(aux == 0){
			if(v[aux] < v[n - 1]) f = 0;
		} else {
			if(v[aux] < v[aux - 1]) f = 0;
		}
		aux = (aux + 1) % n;
	}

	if(f) {
		if(pos == 0) cout << 0 << endl;
		else {
			cout << (n - pos) << endl;
		}
	} else cout << -1 << endl;

	return 0;
}
