#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

vector<vector<int> > adj;
int vis[1005], parent[1005], low[1005], disc[1005];
int cnt_ponte[1005][1005];
int res;

int solve(int u){
    vis[u] = 1;

    static int time = 0;

    disc[u] = low[u] = ++time;
    int nodes = 0;
    for(int v : adj[u]){
        if(!vis[v]){
            parent[v] = u;
            int aux = solve(v);
            low[u] = min(low[u], low[v]);

            if(cnt_ponte[u][v] == 1 and low[v] > disc[u]){
                res = max(res, aux);
            }
            nodes += aux;
        } else if(v != parent[u]){
            low[u] = min(low[u], disc[v]);
        }
    }
    return nodes + 1;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int t; cin >> t;
    while(t--){
        int n, m, c; cin >> n >> m >> c;
        adj.assign(n + 5, vi());
        memset(cnt_ponte, 0, sizeof cnt_ponte);
        for(int i = 0; i < m; i++){
            int x, y; cin >> x >> y;
            cnt_ponte[x][y]++;
            cnt_ponte[y][x]++;
            adj[x].push_back(y);
            adj[y].push_back(x);
        }
        memset(vis, 0, sizeof vis);
        memset(low, 0, sizeof low);
        memset(disc, 0, sizeof disc);
        memset(parent, 0, sizeof parent);
        res = 0;
        solve(c);
        cout << res << endl;
    }
    
    return 0;
}