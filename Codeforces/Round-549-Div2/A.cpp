#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int doors[200005];

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int n; cin >> n;
    int zeros = 0, one = 0;
    for(int i = 0; i < n; i++){
        cin >> doors[i];
        if(doors[i] == 0) zeros++;
        else one++;
    }
    for(int i = 0; i < n; i++){
        if(doors[i] == 0) zeros--;
        else one--;
        if(zeros == 0 or one == 0){
            cout << i + 1 << endl;
            return 0;
        }
    }
    

    
    return 0;
}