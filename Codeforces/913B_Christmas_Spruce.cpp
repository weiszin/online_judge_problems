#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

vvi adj;

int n;

int solve(){
    for(int i = 0; i <= n; i++){
        if(adj[i].size() > 0){
            int cnt = 0;
            for(int v : adj[i]){
                if(adj[v].size() == 0) cnt++;
            }
            if(cnt < 3) return 0;
        }
    }
    return 1;
}

int main(int argc, char const *argv[]){
    ios_base::sync_with_stdio(false);
    cin >> n;
    adj.assign(n + 1, vi());
    for(int i = 1; i < n; i++){
        int x;
        cin >> x;
        x--;
        adj[x].pb(i);
    }
    int f = solve();
    if(f) cout << "Yes" << endl;
    else cout << "No" << endl;

    return 0;
}
