#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef tuple<int,int,int> i3;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n;
    while(cin >> n and n){
        ll res = 0, acc = 0;
        int tempo[1000005];
        memset(tempo, -1, sizeof tempo);
        for(int i = 0; i < n; i++){
            int x; cin >> x;
            if(tempo[x] == -1) res += (x + acc);
            else {
                res += (i - tempo[x]);
            }
            tempo[x] = i;
            acc++;
        }
        cout << res << endl;
    }

    return 0;
}