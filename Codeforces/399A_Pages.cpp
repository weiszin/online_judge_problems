#include <iostream>
#include <map>
#include <cmath>
#include <vector>

using namespace std;

int main(){
    int a, b, c;
    cin >> a >> b >> c;

    vector<int> vet;

    for(int i = b - c; i <= min(b + c, a); i++){
        if(i >= 1) vet.push_back(i);
    }
    if(vet[0] != 1) cout << "<< ";
    for(int i = 1; i <= vet.size(); i++){
        if(i > 1) cout << " ";
        int u = i - 1;
        if(vet[u] == b) cout << "(" << vet[u] << ")";
        else cout << vet[u];
    }
    if(vet.back() != a) cout << " >>" << endl;
    else cout << endl;
        
    return 0;
}