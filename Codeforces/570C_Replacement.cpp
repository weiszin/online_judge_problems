#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min (a,b) ((a) < (b) ? (a) : (b))
#define max (a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

int main(){
    int n, m; cin >> n >> m;
    string s; cin >> s;
    
    int cntSeg = 0, cntPonto = 0;
    int flag = 0;
    for(int i = 0; i < n; i++){
        if(s[i] == '.'){
            cntPonto++;
            flag = 1;
        } else {
            if(flag) cntSeg++, flag = 0;
        }
    }
    if(flag) cntSeg++;



    for(int i = 0; i < m; i++){
        int pos;
        char c;
        cin >> pos >> c;
        if(s.size() == 1) cout << 0 << endl;
        else {
            pos--;
            if(c == '.'){
                if(s[pos] != '.'){
                    int b = 0, f = 0;
                    if(pos - 1 >= 0 and s[pos - 1] == '.') b = 1;
                    if(pos + 1 < n and s[pos + 1] == '.') f = 1;
                    if(b and f) cntPonto++, cntSeg--;
                    else if(b or f) cntPonto++;
                    else cntPonto++, cntSeg++;
                }
            } else {
                if(s[pos] == '.'){
                    int b = 0, f = 0;
                    if(pos - 1 >= 0 and s[pos - 1] == '.') b = 1;
                    if(pos + 1 < n and s[pos + 1] == '.') f = 1;
                    if(b and f) cntPonto--, cntSeg++;
                    else if(b or f) cntPonto--;
                    else cntPonto--, cntSeg--;
                }
            }
            s[pos] = c;
            cout << cntPonto - cntSeg << endl;
        }
    }

    return 0;
}
