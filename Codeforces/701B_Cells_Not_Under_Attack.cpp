#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define PI 2 * acos(0.0)
#define MAX 1000100
#define MOD 1000000007
#define fori(n) for(int i = 0; i < n; i++)
#define forsi(s, n) for(int i = s; i < n; i++)
#define forj(n) for(int j = 0; j < n; j++)
#define forsj(s, n) for(int j = s; j < n; j++)
#define DEBUG(x) cout << #x << " = " << x << endl
#define endl '\n'
#define eb emplace_back

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;
typedef long long ll;

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
	
	ll n, m;
	cin >> n >> m;
	ll r[n + 5], c[n + 5];
	memset(r, 0, sizeof r);
	memset(c, 0, sizeof c);
	ll res = n * n;

	ll totR = 0, totC = 0;
	//vector<ll> v;
	for(int i = 0; i < m; i++){
		ll x, y;
		cin >> x >> y;
		x--; y--;
		if(r[x] == 0) res -= (n - totC), r[x] = 1, totR++;
		if(c[y] == 0) res -= (n - totR), c[y] = 1, totC++;

		cout << res << " ";
	}

	return 0;
}
