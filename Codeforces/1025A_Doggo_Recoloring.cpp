#include <bits/stdc++.h>

using namespace std;

#define INF 0x3f3f3f3f
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#define MAXN 1010
#define MOD 1000000007
#define pb push_back
#define pu push
#define DEBUG(x) cout << #x << " = " << x << endl
#define fst first
#define snd second

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;
typedef vector<vector<ii> > vvii;

typedef long long ll;
typedef unsigned long long ull;

int main(int argc, char const *argv[]){
    int n;
    scanf("%d", &n);
    string s;
    cin >> s;
    int cnt[30];
    memset(cnt, 0, sizeof cnt);
    int f = (n == 1);
    for(char c : s){
        cnt[c - 'a']++;
        if(cnt[c - 'a'] > 1) f = 1;
    }
    if(f) printf("Yes\n");
    else printf("No\n");

    return 0;
}
