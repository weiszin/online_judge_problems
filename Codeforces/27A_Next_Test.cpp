#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;
typedef pair<ll,ll> pll;

int main(){
    int n; cin >> n;
    vector<int> vet;
    for(int i = 0; i < n; i++){
        int x; cin >> x; vet.push_back(x);
    }
    sort(vet.begin(), vet.end());
    for(int i = 1; i <= n; i++){
        if(vet[i - 1] != i){
            cout << i << endl;
            return 0;
        }
    }    
    cout << n + 1 << endl;
    return 0;
}