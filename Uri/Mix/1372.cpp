#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef tuple<int,int,int> iii;
typedef pair<ll,ll> pll;

int n, m;
map<iii, int> dp;
vector<vector<int> > mx;

int solve(int i, int j, int flag){
    if(i >= n) return 0;
    if(j >= m) return (flag ? solve(i + 2, 0, 0) : solve(i + 1, 0, 0));
    if(dp.count(iii(i, j, flag))) return dp[iii(i, j, flag)];
    int res = solve(i, j + 1, flag);
    res = max(res, solve(i, j + 2, 1) + mx[i][j]);
    return dp[iii(i, j, flag)] = res;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    while(cin >> n >> m and (n + m > 0)){
        mx.assign(n + 5, vector<int>());
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                int x; cin >> x;
                mx[i].push_back(x);
            }
        }
        dp.clear();
        cout << solve(0, 0, 0) << endl;
    }
    
    return 0;
}