#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int n;
    while(cin >> n){
        vector<int> vrau;
        for(int i = 0; i < n; i++){
            int x; cin >> x;
            if(!i){
                vrau.push_back(x);
            } else {
                if(x < vrau[0]) vrau[0] = x;
                else if(x > vrau[vrau.size() - 1]) vrau.push_back(x);
                else vrau[(lower_bound(vrau.begin(), vrau.end(), x)-vrau.begin())] = x;
            }
        }
        cout << vrau.size() << endl;
    }
    
    return 0;
}