#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> i2;

int pref[100005], suff[100005];

int main(){
    string s; cin >> s;
    int n = s.size();
    for(int i = 0; i < n; i++){
        if(!i) pref[i] = (s[i] - '0');
        else pref[i] = pref[i - 1] ^ (s[i] - '0');
    }
    
    int flag1 = 1, flag2 = 1;
    int r1 = 1, r2 = 0;
    int ant = s[n - 1] - '0';
    for(int i = n - 2; i >= 1; i--){
        int frente = pref[i - 1];
        if(frente == ant){
            if(s[i] == '0') r1++;
            else flag1 = 0;
            ant ^= 0;
        } else {
            if(s[i] == '1') r1++;
            else flag1 = 0;
            ant ^= 1;
        }
    }
    char last = (ant ^ 0) + '0';
    if(last == s[0]) r1++;
    else flag1 = 0;

    if(flag1) cout << "YES" << endl;
    else cout << "NO" << endl;
    
    return 0;
}