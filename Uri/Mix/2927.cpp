#include <bits/stdc++.h>
using namespace std;

#define INF 0x3f3f3f3
#define PI acos(-1)
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef pair<int,int> ii;
typedef pair<ll,ll> pll;

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    int a, c, x, y;
    cin >> a >> c >> x >> y;
    if(a <= (c - x - y - 1)) cout << "Igor feliz!" << endl;
    else {
        if(x > y / 2) cout << "Caio, a culpa eh sua!" << endl;
        else cout << "Igor bolado!" << endl;
    }
    
    return 0;
}